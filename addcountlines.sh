#!/usr/bin/env bash

DPATH="./accumulated_lines.org"
TMP="./tmp"

LINES=$(./countlines.sh "--out=$TMP")

echo -e "Date: `date`\n" >> $DPATH
cat $TMP >> $DPATH
echo -e "\n\n>>=>>=>>=>>=>>=>>=>>=\n\n" >> $DPATH

rm $TMP
