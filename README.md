# Project

- Librerias usadas con su documentacion bien mona para leer:
  - Lenguajes:
    - Scala: http://scala-lang.org/
    - TypeScript: http://www.typescriptlang.org/
    - CSS3
    - HTML5
    - JavaScript (probablemente)
  - Servidor:
    - Play 2.4.x: https://www.playframework.com/documentation/2.4.x/Home
    - Slick 3.0.x: http://slick.typesafe.com/doc/3.0.3/introduction.html
    - sbt-typescript: https://github.com/ArpNetworking/sbt-typescript
    - play-bootstrap3: http://play-bootstrap3.herokuapp.com/
    - webjars-play: http://www.webjars.org/
  - Cliente:
    - JQuery
    - Bootstrap
    - Font awesome
    - bootstrap-datepicker

- Usamos Play 2.4.x, asi que cuando veais documentacion, arriba siempre cambiad la version por 2.4.x

- How to setup your IDE: https://www.playframework.com/documentation/2.4.x/IDE
    - Generate ensime code:  `activator gen-ensime`
    - Generate eclipse code: `activator eclipse-with-source`

- Comandos:
  - `activator "~run"`: Lanza el server y compila cuando detecta cambios.
  - `activator compile`: Compila el codigo.
  - `activator typescript`: Compila el codigo TypeScript
  - `activator assets`: Genera JS desde TypeScript

- Scala templates: https://www.playframework.com/documentation/2.4.x/ScalaTemplates


# Database

Cuando ejecuteis, probablemente usando h2 para pruebas os saldra:

```
Database 'default' needs evolution!

An SQL script will be run on your database - [Apply this script now!]
```

Simplemente dadle al boton, es para crear la base de datos por defecto, ya que para testing y hasta que estemos al final, usamos h2, una base de datos en memoria.

# Manual testing

Para poder hacer tests manuales a la api, recomiendo instalar [httpie](https://github.com/jkbrzt/httpie).

Por ejemplo, un POST a /api/v1/coches seria:

```
http POST :9000/api/v1/coches modelo=saxo color=azul
```

Como vemos, httpie se encarga de enviarlo como JSON (envia `{"modelo": "saxo", "color": "azul"}`), si se quisiese enviar como un form, simplemente poner `--form` despues de `http` y antes de `POST`.

Hacer un GET es asi de simple:

```
http GET :9000/api/v1/coches
```

# Real testing

Usamos specs2, leed esto para saber como funciona: [https://www.playframework.com/documentation/2.4.x/ScalaTestingWithSpecs2](https://www.playframework.com/documentation/2.4.x/ScalaTestingWithSpecs2)

[test2 matchers](https://etorreborre.github.io/specs2/guide/SPECS2-3.6.5/org.specs2.guide.Matchers.html)

Los tests estan en el directorio `test`

# ScalaJS

[Ejemplo de codigo](https://github.com/hussachai/play-scalajs-showcase)
[Scalatags](lihaoyi.github.io/scalatags) para html tipado.
[ScalaRX](https://github.com/lihaoyi/scala.rx) por si alguien quiere hacer cosas reactive shiats.


# Great examples:

[https://github.com/sbrunk/play-silhouette-slick-seed](https://github.com/sbrunk/play-silhouette-slick-seed)
[https://github.com/playframework/play-slick/tree/master/samples/computer-database](https://github.com/playframework/play-slick/tree/master/samples/computer-database)

# CI Free:

http://shippable.com/ (el que usamos)
https://codeship.com
https://semaphoreci.com


## TODO?

- Use recaptcha? https://github.com/chrisnappin/play-recaptcha/wiki/High-Level-API-%28Play-2.4-and-above%29
