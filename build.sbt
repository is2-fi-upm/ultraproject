import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._
import sbt._
import Process._
import Keys._
import sbt.Project.projectToRef
import com.arpnetworking.sbt.typescript.Import.TypescriptKeys._

// Play 2.4: https://www.playframework.com/documentation/2.4.x/Migration24

// Play 2.4.x injected routes
routesGenerator := InjectedRoutesGenerator

// generate javascript with requirejs
pipelineStages := Seq(rjs)

// Before eclipse, compile to ensure that all the file rae
EclipseKeys.preTasks := Seq(compile in Compile)

resolvers += "scalaz-bintray" at "http://dl.bintray.com/scalaz/releases"

// Dependencies
val webjars = "org.webjars" %% "webjars-play" % "2.4.0-1"
// val bootstrap3 = "com.adrianhurt" %% "play-bootstrap3" % "0.4.4-P24" // http://play-bootstrap3.herokuapp.com/
val fontawesome = "org.webjars" % "font-awesome" % "4.3.0-2"
val datepicker = "org.webjars" % "bootstrap-datepicker" % "1.4.0"
val slick = "com.typesafe.play" %% "play-slick" % "1.1.1"
val slickevolutions = "com.typesafe.play" %% "play-slick-evolutions" % "1.1.1"
val h2 = "com.h2database" % "h2" % "1.4.189"
val recaptcha = "com.nappin" %% "play-recaptcha" % "1.5"

val play2auth = "jp.t2v" %% "play2-auth" % "0.14.1"
val play2authsocial = "jp.t2v" %% "play2-auth-social" % "0.14.1" // for social login
val play2authtest = "jp.t2v" %% "play2-auth-test" % "0.14.1" % "test"

val scalaz = "org.scalaz" %% "scalaz-core" % "7.1.5"

// The common settings
lazy val commonSettings = Seq(
  version := "0.1.0",
  scalaVersion := "2.11.7"
)

lazy val clients = Seq(client)

lazy val server = (project in file("server")).
  settings(commonSettings: _*).
  settings(routesGenerator := InjectedRoutesGenerator).
  settings(routesImport += "binders.Binders._").
  settings(javaOptions in Test += "-Dslick.dbs.default.connectionTimeout=30 seconds").
  settings(scalaJSProjects := clients,
    name := "server",
    pipelineStages := Seq(scalaJSProd, gzip),
    jacoco.settings, //this is the important part.
    //coverageEnabled := true,
    //coverageExcludedPackages := "<empty>;Reverse.*;.*AuthConfigImpl.*;views.html;router",
    resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases",
    libraryDependencies ++= Seq(
      "com.vmunier" %% "play-scalajs-scripts" % "0.3.0",
      webjars,
      "org.webjars.npm" % "jquery" % "2.1.4",
      "org.webjars" % "bootstrap" % "3.3.5",
      "org.webjars.npm" % "bootstrap-select" % "1.7.4",
      slick,
      slickevolutions,
      h2,
      recaptcha,
      // bootstrap3,
      fontawesome,
      datepicker,
      play2auth,
      play2authsocial,
      play2authtest,
      play.sbt.Play.autoImport.cache, // only when you use default IdContainer
      ws,
      specs2 % Test
    ),
    scalacOptions ++= Seq("-feature")
  ).settings(
    parallelExecution in jacoco.Config := false
  ).settings(
    // the name of the jar to generate and other things for the package
    assemblyJarName in assembly := "is2.jar",
    test in assembly := {},
    mainClass in assembly := Some("play.core.server.NettyServer"),
    fullClasspath in assembly += Attributed.blank(PlayKeys.playPackageAssets.value),
    assemblyMergeStrategy in assembly := {
      case PathList(ps @ _*) if ps.last == "ServerWithStop.class" => MergeStrategy.first
      case x =>
        val oldStrategy = (assemblyMergeStrategy in assembly).value
        oldStrategy(x)
    }
  ).enablePlugins(PlayScala).
  aggregate(clients.map(projectToRef): _*).
  dependsOn(sharedJvm)

lazy val client = (project in file("client")).
  settings(commonSettings: _*).
  settings(
    scalaJSStage in Global := FastOptStage,
    // jsDependencies += RuntimeDOM % "test",  // Dat Rhino
    scalacOptions ++= Seq("-feature"),
    persistLauncher := true,
    persistLauncher in Test := false,
    libraryDependencies ++= Seq(
      "org.scala-js" %%% "scalajs-dom" % "0.8.1",
      "com.lihaoyi" %%% "scalatags" % "0.5.2",
      "com.lihaoyi" %%% "scalarx" % "0.2.8",
      "com.lihaoyi" %%% "upickle" % "0.3.6",
      "be.doeraene" %%% "scalajs-jquery" % "0.8.1",
      "org.monifu" %%% "monifu" % "1.0-RC3",
      "com.github.japgolly.fork.scalaz" %%% "scalaz-core" % "7.1.2"
      // "com.lihaoyi" %%% "upickle" % "0.3.4"
    )
  ).enablePlugins(ScalaJSPlugin, ScalaJSPlay).
  dependsOn(sharedJs)


lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared")).
  settings(commonSettings: _*).
  jsConfigure(_ enablePlugins ScalaJSPlay).
  settings(scalacOptions ++= Seq("-feature")).
  jsSettings(sourceMapsBase := baseDirectory.value / "..")

// lazy val shared = (crossProject.crossType(CrossType.Pure) in file("shared")).
//   settings(commonSettings: _*).
//   jsConfigure(_ enablePlugins ScalaJSPlay)

lazy val sharedJvm = shared.jvm
lazy val sharedJs = shared.js

// loads the Play project at sbt startup
onLoad in Global := (Command.process("project server", _: State)) compose (onLoad in Global).value

// for Eclipse users
EclipseKeys.skipParents in ThisBuild := false
EclipseKeys.preTasks := Seq(compile in (server, Compile))

// // main application
// lazy val root = project.in(file(".")).
//   settings(commonSettings: _*).
//   settings(javaOptions in Test += "-Dslick.dbs.default.connectionTimeout=30 seconds").
//   settings(
//     name := "is2",
//     libraryDependencies ++= Seq(
//       slick,
//       slickevolutions,
//       h2,
//       webjars,
//       bootstrap3,
//       fontawesome,
//       datepicker,
//       specs2 % Test
//     ),
//     scalacOptions ++= Seq("-feature")
//   ).enablePlugins(PlayScala, SbtWeb).settings(
//     // Typescript options
//     JsEngineKeys.engineType := JsEngineKeys.EngineType.Node,
//     moduleKind := "commonjs",
//     sourceMap := true,
//     noImplicitAny := true
//   )
