#!/usr/bin/env bash

cloc $1 "client/src" "server/app" "server/conf" "server/test" "public" "shippable.yml" "scalastyle-config.xml" "README.md" "LICENSE" "build.sbt" "shared/src/main"
