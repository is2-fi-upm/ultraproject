package shared.classes

import scala.language.implicitConversions

sealed abstract class TipoUsuario
object TipoUsuario {
  case object Anonimo extends TipoUsuario
  case object Normal extends TipoUsuario
  case object Empresa extends TipoUsuario
  case object Empleado extends TipoUsuario
  case object Admin extends TipoUsuario

  implicit def str2TipoUsuario(str: String): TipoUsuario = str.toLowerCase() match {
    case "anonimo" => Anonimo
    case "normal" => Normal
    case "empresa" => Empresa
    case "empleado" => Empleado
    case "admin" => Admin
    case _ => Normal
  }

  implicit def tipousuario2Str(u: TipoUsuario): String = u match {
    case Anonimo => "anonimo"
    case Normal => "normal"
    case Empresa => "empresa"
    case Empleado => "empleado"
    case Admin => "admin"
  }
}
