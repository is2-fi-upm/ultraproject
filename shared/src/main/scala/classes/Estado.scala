package shared.classes

import scala.language.implicitConversions

sealed abstract class Estado
object Estado {
  case object Disponible extends Estado
  case object Reservado extends Estado
  case object Carretera extends Estado
  case object Reparacion extends Estado
  case object Baja extends Estado

  implicit def str2Estado(str: String): Estado = str.toLowerCase() match {
    case "disponible" => Disponible
    case "reservado" => Reservado
    case "carretera" => Carretera
    case "reparacion" => Reparacion
    case "baja" => Baja
    case _ => Baja
  }

  implicit def estado2Str(e: Estado): String = e match {
    case Disponible => "disponible"
    case Reservado => "reservado"
    case Carretera => "carretera"
    case Reparacion => "reparacion"
    case Baja => "baja"
  }
}
