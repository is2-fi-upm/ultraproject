package shared.classes

import scala.language.implicitConversions

sealed abstract class Metodo
object Metodo {
  case object Metalico extends Metodo
  case object Mastercard extends Metodo
  case object Visa extends Metodo
  case object AmericanExpress extends Metodo

  implicit def str2Metodo(str: String): Metodo = str.toLowerCase() match {
    case "metalico" => Metalico
    case "mastercard" => Mastercard
    case "visa" => Visa
    case "american express" => AmericanExpress
    case _ => Metalico
  }

  implicit def metodo2Str(r: Metodo): String = r match {
    case Metalico => "Metalico"
    case Mastercard => "Mastercard"
    case Visa => "Visa"
    case AmericanExpress => "American Express"
  }
}
