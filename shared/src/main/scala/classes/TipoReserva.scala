package shared.classes

import scala.language.implicitConversions

sealed abstract class TipoReserva
object TipoReserva {
  case object Km extends TipoReserva
  case object Dia extends TipoReserva
  case object Semana extends TipoReserva
  case object FinSemana extends TipoReserva

  implicit def str2TipoReserva(str: String): TipoReserva = str.toLowerCase() match {
    case "km" => Km
    case "dia" => Dia
    case "semana" => Semana
    case "finsemana" => FinSemana
    case _ => Dia
  }

  implicit def tiporeserva2str(r: TipoReserva): String = r match {
    case Km => "km"
    case Dia => "dia"
    case Semana => "semana"
    case FinSemana => "finsemana"
  }
}
