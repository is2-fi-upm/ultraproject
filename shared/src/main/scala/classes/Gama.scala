package shared.classes

import scala.language.implicitConversions

sealed abstract class Gama
object Gama {
  case object Alta extends Gama
  case object Media extends Gama
  case object Baja extends Gama

  implicit def str2Gama(str: String): Gama = str.toLowerCase() match {
    case "alta" => Alta
    case "media" => Media
    case "baja" => Baja
    case _ => Baja
  }

  implicit def gama2Str(g: Gama): String = g match {
    case Alta => "alta"
    case Media => "media"
    case Baja => "baja"
  }
}
