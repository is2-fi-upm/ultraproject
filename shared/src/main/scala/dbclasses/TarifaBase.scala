package shared.dbclasses

import shared.classes.Gama

case class TarifaBase(
  gama: Gama,
  precioKm: Float,
  precioDia: Float,
  precioSemana: Float,
  precioFinSemana: Float)
