package shared.dbclasses

import java.sql.{Timestamp, Date}
import shared.classes.TipoReserva

case class Reserva(
  id: Option[Int],
  idusuario: String,
  idreservante: String,
  idcoche: String,
  tarjeta: String,
  caducidadtarjeta: Date,
  tipotarjeta: String,
  fechareserva: Timestamp,
  fechainicio: Date,
  tiporeserva: TipoReserva,
  precio: Float)
