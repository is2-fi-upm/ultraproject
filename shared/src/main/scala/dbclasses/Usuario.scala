package shared.dbclasses

import shared.classes.TipoUsuario

case class Usuario(
  dni: String,
  nombre: String,
  apellidos: String,
  tipo: TipoUsuario,
  email: String,
  pass: String
)
