package shared.dbclasses

import java.sql.Date
import shared.classes.Metodo

case class Pago(
  id: Option[Int],
  fecha: Date,
  reserva: Int,
  metodo: Metodo,
  coste: Float)
