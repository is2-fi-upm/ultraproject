package shared.dbclasses

import java.sql.Timestamp
import shared.classes.Metodo

case class Factura(
  id: Option[Int],
  nombre: String,
  dni: String,
  fecha: Timestamp,
  coche: String,
  precio: Float,
  metodo: Metodo,
  franquicia: Int)
