package shared.dbclasses

import java.sql.Date

case class TarifaPeriodo(
  nombre: String,
  inicio: Date,
  fin: Date,
  factor: Float)
