package shared.dbclasses

case class Extra(
  nombre: String,
  precio: Float,
  descripcion: String)
