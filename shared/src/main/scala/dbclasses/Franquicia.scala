package shared.dbclasses

case class Franquicia(
  id: Option[Int],
  calle: String,
  numero: Int,
  codigopostal: Int,
  poblacion: String,
  comunidad: String,
  pais: String)
