package shared.dbclasses

import java.sql.Blob
import shared.classes.{Gama, Estado}

case class Coche(
  matricula: String,
  marca: String,
  modelo: String,
  gama: Gama,
  foto: String,
  estado: Estado,
  reserva: Int
)

// case class Coche(
//   matricula: String,
//   marca: String,
//   modelo: String,
//   gama: Gama,
//   foto: Blob,
//   estado: Estado,
//   reserva: Int
// )
