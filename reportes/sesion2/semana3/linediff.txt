develop origin/develop e3f85e1438a6e6bf4ec273c8e86c20494925c7d9
Author:     Rock Neurotiko <miguelglafuente@gmail.com>
AuthorDate: Wed Dec 16 23:48:38 2015 +0100
Commit:     Rock Neurotiko <miguelglafuente@gmail.com>
CommitDate: Wed Dec 16 23:48:38 2015 +0100

Parent:     3e56eb2 Add report
Merged:     develop master
Containing: develop


12 files changed, 324 insertions(+), 62 deletions(-)
build.sbt                                          |   4 +-
client/src/main/scala/Gestion.scala                | 190 +++++++++++++++++----
client/src/main/scala/jsclasses/Reserva.scala      |   6 +
server/app/controllers/AwesomeLoggedApp.scala      |   5 +-
server/app/controllers/GestionarFranquicia.scala   |  72 +++++++-
.../app/controllers/api/v1/CocheApplication.scala  |   4 +-
.../controllers/api/v1/ReservaApplication.scala    |  20 ++-
server/app/tables/CocheTable.scala                 |  24 ++-
server/app/tables/PagosTable.scala                 |  24 ++-
server/conf/evolutions/default/2.sql               |  17 +-
server/conf/routes                                 |   4 +-
shared/src/main/scala/classes/Metodo.scala         |  16 +-
