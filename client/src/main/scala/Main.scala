package example

import scala.scalajs.js
import org.scalajs.dom
import org.scalajs.jquery.{jQuery=>$,_}
import js.Dynamic.{ global => g }

object ScalaJSExample extends js.JSApp {
  def center(outer: js.Any, content: String) = {
    $(outer).resize(() => {
      $(content).css("position", "absolute")
      $(content).css("left", ($(outer).width() - $(content).outerWidth())/2)
      $(content).css("top", ($(outer).height() - $(content).outerHeight())/2)
    })
  }

  def main(): Unit = {
    // println("YEAH")
    // dom.document.getElementById("scalajsShoutOut").textContent = SharedMessages.itWorks
    val content = "#content"
    val document = g.document
    val window = g.window

    $(document).ready(() =>{
      // center(window, content)
      $(window).resize()
    })
  }
}
