package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import js.Dynamic.{ global => g }
import org.scalajs.jquery.{jQuery=>$,_}
import jsclasses.{Franquicia, Coche}
import shared.classes.{TipoReserva, Estado, Gama}
import TipoReserva._
import Estado._
import Gama._
import example.helpers.Implicits._
import example.helpers.Methods._

@JSExport
object Profile  {
  def ui() = div(`class`:="container",
    div(`class`:="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad", style:="margin-top: 1em;",
      div(`class`:="panel panel-info",
        div(`class`:="panel-heading",
          h3(`class`:="panel-title", "Perfil")
        ),
        div(`class`:="panel-body",
          div(`class`:="row",
            // div(`class`:="col-md-3 col-lg-3", "align".attr:="center", img(id:="cocheImage", alt:="", src:="", `class`:="img-thumbnail img-responsive"),
            //   label(id:="cocheName", "")
            // ),
            div(`class`:=" col-md-9 col-lg-9 ",
              table(`class`:="table table-user-information",
                tbody(
                  tr(
                    td("Nombre"),
                    td(id:="nombreField", "")
                  ),
                  tr(
                    td("Apellidos"),
                    td(id:="apellidosField", "")
                  ),
                  tr(
                    td("DNI"),
                    td(id:="dniField", "")
                  ),
                  tr(
                    td("Tipo de usuario"),
                    td(id:="tipoField", "")
                  ),
                  tr(
                    td("Email"),
                    td(id:="emailField", "")
                  )
                )
              )
            )
          ),
          div(`class`:="row",
            div(style:="text-align: center;",
              div(style:="display: inline-block; width: 100%;",
                label("Reservas:"),
                div(id:="reservasContainer")
                // a(href:="#", id:="aceptarBtn", `class`:="btn btn-success btn-lg", "Aceptar"),
                // a(href:="#", id:="cancelarBtn", `class`:="btn btn-default btn-lg", "Cancelar")
              )
            )
          )
        )
      )
    )
  )

  case class Reserv(id: Int, coche: String, inicio: Double, tipo: String, precio: Double)

  def pDate(d: js.Date) = s"${d.getDate()}/${d.getMonth()}/${d.getFullYear()}"

  def truncate(num: Double) = Math.round(num * 100) / 100

  def reservaUI(r: Reserv) = {
    div(`class`:="col-xs-5 toppad", style:="margin-top: 1em;",
      div(`class`:="panel panel-info",
        div(`class`:="panel-heading",
          h3(`class`:="panel-title", s"${r.coche}")
        ),
        div(`class`:="panel-body",
          div(`class`:="row",
            div(`class`:=" col-md-9 col-lg-9 ",
              table(`class`:="table table-user-information",
                tbody(
                  tr(
                    td("Fecha"),
                    td(s"${pDate(new js.Date(r.inicio))}")
                  ),
                  tr(
                    td("Tipo"),
                    td(s"${r.tipo}")
                  ),
                  tr(
                    td("Precio"),
                    td(s"${truncate(r.precio)}€")
                  )
                )
              )
            ),
            div(`class`:="row",
              div(style:="text-align: center;",
                div(style:="display: inline-block; width: 100%;",
                  a(href:=s"/reserva/${r.id}", id:="verBtn", `class`:="btn btn-success btn-md", "Ver")
                )
              )
            )
          )
        )
      )
    )
  }

  @JSExport
  def main(nombre: String, apellidos: String, dni: String, tipo: String, email: String) = {
    $.getJSON(s"/api/v1/reservas/usuario", success = (x: js.Any) => {
      val l = readDynamic[List[Reserv]](x)
      $("#reservasContainer").append((l map reservaUI).render)
    })
    $("#content").append(ui().render)
    $("#nombreField").text(nombre)
    $("#apellidosField").text(apellidos)
    $("#dniField").text(dni)
    $("#tipoField").text(tipo)
    $("#emailField").text(email)
  }
}
