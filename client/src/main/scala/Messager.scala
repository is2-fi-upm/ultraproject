package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import js.Dynamic.{ global => g }
import org.scalajs.jquery.{ jQuery=>$, _}
import upickle.Js
import upickle.{default => upickle}
import jsclasses.{Franquicia}
import shared.classes.{Estado, Gama}
import Estado._
import Gama._
import shared.dbclasses.Coche
import example.helpers.Implicits._
import example.helpers.Methods._
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future

@JSExport
object Messager {
  def base(extra: String, msg: TypedTag[org.scalajs.dom.raw.HTMLElement]) =
    div(`class`:=s"alert alert-$extra alert-dismissable",
      button(`type`:="button",
        `class`:="close",
        data("dismiss"):="alert",
        "aria-hidden".attr:="true", "×"),
      msg
    )

  def addToGroup(m: TypedTag[org.scalajs.dom.raw.HTMLElement], group: String = ".alert-group") =
    $(group).append(m.render)

  @JSExport
  def createAlert(msg: String) = base("danger", strong(msg))

  @JSExport
  def alert(msg: String) = addToGroup(base("danger", strong(msg)))

  @JSExport
  def info(msg: String) = addToGroup(base("info", label(msg)))
}
