package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import js.Dynamic.{ global => g }
import org.scalajs.jquery.{jQuery=>$,_}

@JSExport
object Login {
  def center(outer: js.Any, content: String) = {
    $(content).css("position", "absolute")
    if ($(outer).width() >= 600) {
      $(content).css("width", "600")
    }
    $(content).css("left", ($(outer).width() - $(content).outerWidth())/2)
    $(content).css("top", ($(outer).height() - $(content).outerHeight())/2)
  }

  def createField(lbl: String, fieldname: String, typ: String = "text", auto: String = "on") = div(`class`:="login-field-wrap",
    label(`class`:="login-label", lbl, span(`class`:="req", "*")),
    input(`class`:="login-input", name:=fieldname, `type`:=typ, required, autocomplete:=auto)
  )

  @JSExport
  def main(errortext: String = "", successtext: String = "") = {
    val labeldos = "labeldos".tag[dom.html.Label]

    val signupHTML = div(id:="signup",
      form(action:="/signup", method:="POST",
        div(`class`:="login-top-row",
          createField("Nombre", "nombre"),
          createField("Apellidos", "apellidos")
        ),
        createField("DNI", "dni"),
        createField("Email", "email"),
        createField("Password", "password", "password", "off"),
        createField("Confirm Password", "passwordConfirm", "password", "off"),
        div(`class`:="login-field-wrap2",
          label(`class`:="login-label2", "Tipo de cliente", span(`class`:="req", "*")),
          select(`class`:="custom-select", name:="tipoCliente",
            // optgroup(
              option(value:="normal", "Particular"),
              option(value:="empresa", "Empresa")
            // )
          )
        ),
        button(`type`:="submit", `class`:="login-button login-button-block", "Registrate")
      )
    )

    val loginHTML = div(id:="login",
      h1(`class`:="login-text-h1", "Bienvenido de nuevo!"),
      form(action:="/login", method:="POST",
        createField("DNI", "dni"),
        createField("Contraseña", "password", "password", "off"),
        // p(`class`:="login-forgot", a(href:="#", "Forgot Password?"))
        button(`class`:="login-button login-button-block", "Entrar")
      )
    )

    val formHTML = div(`class`:="login-form",
      ul(
        `class`:="login-tab-group",
        li(`class`:="tab active", a(href:="#signup", "Registrarse")),
        li(`class`:="tab", a(href:="#login", "Iniciar sesion"))
      ),
      div(`class`:="login-tab-content",
        signupHTML,
        loginHTML
      )
    )

    $("#content").append(formHTML.render)

    $(".login-form").find("input, textarea").on("keyup blur focus", (e: JQueryEventObject) => {
      val $this = $(e.target)
      val label = $this.prev("label")
      val isempty = $this.`val`().asInstanceOf[String] == ""
      val classes = e.`type` match {
        case "keyup" => if (isempty) "active highlight" else "active highlight"
        case "blur" => if (isempty) "active highlight" else "highlight"
        case "focus" => if (isempty) "highlight" else "highlight"
        case _ => ""
      }
      if (isempty)
        label.removeClass(classes)
      else
        label.addClass(classes)
    })

    $(".tab a").on("click", (e: JQueryEventObject) => {
      e.preventDefault()
      val $this = $(e.target)
      val target = $this.attr("href")
      $this.parent().addClass("active").siblings().removeClass("active")
      $(".login-tab-content > div").not(target).hide()
      $(target).fadeIn(600)
    })

    $("#content").resize(() => {
      center("#content", ".login-form")
    })

    val url = dom.window.location.href.split("#")
    if (url.length > 1) {
      url(1) match {
        case "login" => {
          val l = $("#login")
          $(".login-tab-content > div").not(l).hide()
          $(".tab.active").removeClass("active").siblings().addClass("active")
          l.fadeIn(600)
          $("#login .login-input").first().focus()
        }
        case _ =>
      }
    }

  }
}
