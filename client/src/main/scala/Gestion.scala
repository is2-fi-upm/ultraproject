package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import js.Dynamic.{ global => g }
import org.scalajs.jquery.{ jQuery=>$, _}
import upickle.Js
import upickle.{default => upickle}
import jsclasses.{Franquicia, Reserva}
import shared.classes.{Estado, Gama, Metodo}
import Estado._
import Gama._
import Metodo._
import shared.dbclasses.Coche
import example.helpers.Implicits._
import example.helpers.Methods._
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future
import scalaz._
import Scalaz.{id => _, option => _, _}

@JSExport
object Gestion {
  var globalCoches: List[Coche] = List()
  var globalMatriculas: List[String] = List()
  val metodos: List[String] = List(Metalico, Mastercard, Visa, AmericanExpress)
  val gamas: List[String] = List(Alta, Media, Gama.Baja)
  val estados: List[String] = List(Disponible, Reservado, Carretera, Reparacion, Estado.Baja)
  val estadosNames = List("Disponible", "Reservado", "En carretera", "En reparacion", "De baja")

  case class Recv(coche: String, factura: Int)

  def nonEmpty[T: Monoid](t: T, extra: String) = {
    val m = implicitly[Monoid[T]]
    if (m.zero == t)
      s"No hay $extra".failureNel[T]
    else
      t.successNel[String]
  }

  def makePayment(x: dom.Event): Unit = {
    val matriculaRaw = $("#cocheForm").`val`().asInstanceOf[String]
    val reservaRaw = $("#reservaPayment").text()
    val costeRaw = $("#costePayment").text()
    val metodoRaw = $("#metodoForm").`val`().asInstanceOf[String]
    val facturaRaw = $("#crearFactura").is(":checked")
    val nombreRaw = $("#nombreFactura").text()
    val dniRaw = $("#nifFactura").text()

    val matriculaV = nonEmpty(if (matriculaRaw == null) "" else matriculaRaw, "matricula")
    val reservaV = nonEmpty(reservaRaw, "reserva")
    val costeV = nonEmpty(costeRaw, "coste")
    val metodoV = nonEmpty(metodoRaw, "metodo")
    val nombreV = nonEmpty(nombreRaw, "nombre")
    val dniV = nonEmpty(dniRaw, "NIF")

    (matriculaV |@| reservaV |@| costeV |@| metodoV) { _ + _ + _ + _ } match {
      case Failure(x) => x foreach { Messager.alert(_) }
      case Success(_) => {
        val formD = new dom.FormData()
        formD.append("matricula", matriculaRaw)
        formD.append("reserva", reservaRaw)
        formD.append("coste", costeRaw)
        formD.append("metodo", metodoRaw)
        formD.append("factura", facturaRaw)

        formD.append("nombre", nombreRaw)
        formD.append("dni", dniRaw)

        $.ajax(js.Dynamic.literal(
          url = "/gestionar/createPayment",
          data = formD,
          processData = false,
          contentType = false,
          `type` = "POST",
          success = (x: js.Any) => {
            val matrRecv = readDynamic[Recv](x)
            getAndAddCar(matrRecv.coche)
            g.$("#payModal").modal("hide")
          },
          error = () => Messager.alert("Some error doing the request")
        ).asInstanceOf[JQueryAjaxSettings])

        // $.ajax(js.Dynamic.literal(
        //   url = "/gestionar/createPayment",
        //   `type` = "POST",
        //   // data = js.JSON.stringify(js.Dynamic.literal(
        //   //   matricula = matriculaRaw,
        //   //   reserva = reservaRaw,
        //   //   coste = costeRaw,
        //   //   metodo = metodoRaw,
        //   //   factura = facturaRaw
        //   // )),
        //   data = formD,
        //   // contentType = "application/json",
        //   // dataType = "json",
        //   contentType = false,
        //   dataType = false,
        //   success = (data: js.Any) => {
        //     val matrRecv = readDynamic[String](data)
        //     println(matrRecv)
        //     // getAndAddCar(matrRecv)
        //     g.$("#addCarModal").modal("hide")
        //   }
        // ).asInstanceOf[JQueryAjaxSettings])
      }
    }
  }

  def addCarSubmit(x: dom.Event) = {
    val formD = new dom.FormData()
    val matriculaRaw = $("#matriculaForm").`val`().asInstanceOf[String]
    val marcaRaw = $("#marcaForm").`val`().asInstanceOf[String]
    val modeloRaw = $("#modeloForm").`val`().asInstanceOf[String]
    val gama = $("#gamaForm").`val`().asInstanceOf[String]
    val fileList = $("#fotoForm").prop("files").asInstanceOf[dom.FileList]

    val matV = nonEmpty(matriculaRaw, "matricula")
    val marcaV = nonEmpty(marcaRaw, "marca")
    val modeloV = nonEmpty(modeloRaw, "modelo")
    val fileV = if (fileList.length == 0)
      "No hay foto".failureNel[dom.FileList]
    else fileList.successNel[String]

    // Validateeee
    (matV |@| marcaV |@| modeloV |@| fileV) { _ + _ + _ + _} match {
      case Failure(x) => x foreach { Messager.alert(_) }
      case Success(_) => {
        formD.append("matricula", matriculaRaw)
        formD.append("marca", marcaRaw)
        formD.append("modelo", modeloRaw)
        formD.append("gama", gama)
        formD.append("foto", fileList(0))

        $.ajax(js.Dynamic.literal(
          url = "/gestionar/createCar",
          data = formD,
          processData = false,
          contentType = false,
          `type` = "POST",
          success = (data: js.Any) => {
            val matrRecv = readDynamic[String](data)
            getAndAddCar(matrRecv)
            g.$("#addCarModal").modal("hide")
          }
        ).asInstanceOf[JQueryAjaxSettings])
      }
    }
  }

  def getAndAddCar(matr: String) = {
    if ($(s"#$matr").length == 0) {
      $.getJSON(s"/api/v1/coches/$matr", success = (x: js.Any) => {
        val coche = readDynamic[Coche](x)
        $("#innerContent").append(generateCocheUI(coche).render)
        g.$(s"#${matr}Estado").selectpicker()
      })
    } else {
      g.$(s"#${matr}Estado").selectpicker("val", "disponible")
    }
  }

  def createSelector(idS: String, ops: List[(String, String)], f: String => Unit, nameS: String = "", search: Boolean = false) =
    select(
      id:=idS,
      `class`:="selectpicker col-md-12",
      if (search) "data-live-search".attr:="true",
      name:=nameS,
      onchange:= {() => {
        val selected = $(s"#${idS}").`val`().asInstanceOf[String]
        f(selected)
      }},
      ops map { case (x, y) => option(value:=x, y)}
    )

  def createEstadoSelector(matr: String, est: Estado, f: String => Unit) = {
    def toS(e: Estado): String = e
    createSelector(s"${matr}Estado", estados zip estadosNames, f)
  }

  def changeEstado(matr: String): String => Unit = (newEstado: String) => {
    $(s"#${matr} .caption").find("p").last().text(s"$newEstado".capitalize)
    $.ajax(js.Dynamic.literal(
      url = s"/api/v1/coches/$matr",
      data = js.JSON.stringify(js.Dynamic.literal(estado = newEstado)),
      contentType = "application/json; charset=utf-8",
      dataType = "json",
      `type`= "PUT",
      error = (jqXHR: js.Any, textStatus: String, error: String) => {
        Messager.alert("Some error...")
      },
      success = (data: js.Any) => {
        getMatriculasReservadas()
        Messager.info(s"Estado de $matr cambiado a $newEstado")
      }
    ).asInstanceOf[JQueryAjaxSettings])
  }

  def generateCocheUI(c: Coche) = {
    li(`class`:="col-xs-3 cocheTarjeta",
      style:="margin-bottom: 5px;",
      id:=c.matricula,
      div(`class`:="thumbnail", style:="padding: 0; height: 100%;",
        div(style:="padding: 4px;",
          img(alt:=c.matricula, style:="width: 100%;",
            src:=s"/assets/cars/${c.foto}"
          )
        ),
        div(`class`:="caption",
          h2(s"${c.marca} ${c.modelo}"),
          p(s"Matricula ${c.matricula}"),
          p(s"Gama ${c.gama}"),
          p(i(`class`:="icon icon-map-marker"), s"${c.estado}")
        ),
        div(`class`:="modal-footer",
          style:="text-align: center;",
          createEstadoSelector(c.matricula, c.estado, changeEstado(c.matricula))
        )
      )
    )
  }

  def genericModal(title: String, tid: String, form: TypedTag[org.scalajs.dom.raw.HTMLFormElement], textButton: String, idButton: String, f: dom.Event => Unit) = {
    div(`class`:="modal fade", id:=tid, tabindex:="-1", role:="dialog", "aria-labelledby".attr:="modalLabel", "aria-hidden".attr:="true",
      div(`class`:="modal-dialog",
        div(`class`:="modal-content",
          div(`class`:="modal-header",
            button(`type`:="button", `class`:="close", data("dismiss"):="modal",
              span("aria-hidden".attr:="true", "×"), span(`class`:="sr-only", "Close")),
              h3(`class`:="modal-title", id:="lineModalLabel", title)
          ),
          div(`class`:="modal-body",
            form
          ),
          div(`class`:="modal-footer",
            div(`class`:="btn-group btn-group-justified", role:="group", "aria-label".attr:="group button",
              div(`class`:="btn-group", `role`:="group",
                button(`type`:="button", id:=idButton, `class`:="btn btn-success btn-hover-green", data("action"):="save", role:="button", textButton, onclick:=f)
              )
            )
          )
        )
      )
    )
  }

  def hideByText(e: JQueryEventObject): js.Any = {
    def carText(c: Coche) = s"${c.matricula}${c.marca}${c.modelo}${c.gama}${c.estado}"
    val text = $("#filtrarText").`val`().asInstanceOf[String]
    text.replaceAll(" ", "").toLowerCase() match {
      case "" => globalCoches map { car => $(s"#${car.matricula}").fadeIn(50)}
      case t => {
        globalCoches map {car =>
          if (carText(car).replaceAll(" ", "").toLowerCase().contains(t))
            $(s"#${car.matricula}").fadeIn(50)
          else
            $(s"#${car.matricula}").fadeOut(50)
        }
      }
    }
  }

  val header = div(`class`:="container col-md-12",
    style:="height: 4em; margin-top: 3px; margin-bottom: 10px; border: 1px solid black;",
    div(`class`:="row", style:="display: flex; align-items: center; justify-content: center;",
      button(`class`:="btn btn-success", data("toggle"):="modal", data("target"):="#addCarModal", style:="margin: 5px;", "Añadir coche"),
      button(`class`:="btn btn-success", data("toggle"):="modal", data("target"):="#payModal", style:="margin: 5px;", "Hacer pago"),
      input(`class`:="form-control login-input", "id".attr:="filtrarText",
        placeholder:="Filtrar por texto", style:="color: black; width: 30%;")
    )
  )

  val innerContent = ul(`class`:="thumbnails col-md-12 list-unstyled",
    id:="innerContent",
    style:="height: 100%; border: 1px solid black; display: flex; flex-flow: row wrap; padding-top: 5px; padding-bottom: 5px;"
  )

  val addForm = form(
    div(`class`:="form-group",
      label(`for`:="matriculaForm", "Matricula"),
      input(`type`:="text", `class`:="form-control", id:="matriculaForm", name:="matricula", placeholder:="Matricula")
    ),
    div(`class`:="form-group",
      label(`for`:="marcaForm", "Marca"),
      input(`type`:="text", `class`:="form-control", id:="marcaForm", name:="marca", placeholder:="Marca")
    ),
    div(`class`:="form-group",
      label(`for`:="modeloForm", "Modelo"),
      input(`type`:="text", `class`:="form-control", id:="modeloForm", name:="modelo", placeholder:="Modelo")
    ),
    div(`class`:="form-group",
      label(`for`:="gamaForm", "Gama"),
      createSelector("gamaForm", gamas zip gamas, x => {}, "gama")
    ),
    div(`class`:="form-group",
      label(`for`:="fotoForm", "Foto"),
      input(`type`:="file", id:="fotoForm")
        // p(`class`:="help-block", "Example block-level help text here.")
    )
  )

  def getReservaData(matricula: String) = $.get(s"/api/v1/reservas/coche/$matricula", success = (x: js.Any) => {
    val reservaObj = readDynamic[Reserva](x)
    $("#reservaPayment").text(reservaObj.reserva.toString)
    $("#costePayment").text((js.Math.round(reservaObj.precio * 100) / 100).toString)
  })

  def paymentForm = form(
    div(`class`:="form-group",
      label(`for`:="cocheForm", "Matricula del coche"),
      createSelector("cocheForm", globalMatriculas zip globalMatriculas, getReservaData, "coche", true),
      div(style:="text-align: center; margin-top: 5px;",
        div(style:="display: inline-block; width: 100%;",
          label(style:="margin-top:-10px;","Reserva"),
          br(),
          label(style:="font-size: 4em; margin-top: -0.3em;", id:="reservaPayment", "")
        )),
      div(style:="text-align: center; margin-top: 5px;",
        div(style:="display: inline-block; width: 100%;",
          label(style:="margin-top:-10px;","Coste"),
          br(),
          label(style:="font-size: 4em; margin-top: -0.3em;", id:="costePayment", ""),
          label(style:="font-size: 4em; margin-top: -0.3em;", "€")
        )),
      label(`for`:="metodoForm", "Metodo de pago"),
      createSelector("metodoForm", metodos zip metodos, x => {}, "metodo", false),
      div(`class`:="form-group", style:="margin-top: 5px;",
        input(`type`:="checkbox", id:="crearFactura", name:="factura", style:="margin-right: 5px;"),
        label(`for`:="gamaForm", "Crear factura")
      ),
      div(`class`:="form-group",
        label(`for`:="matriculaForm", "Nombre factura"),
        input(`type`:="text", `class`:="form-control", id:="nombreFactura", name:="nombre", placeholder:="Nombre", disabled)
      ),
      div(`class`:="form-group",
        label(`for`:="matriculaForm", "NIF"),
        input(`type`:="text", `class`:="form-control", id:="nifFactura", name:="dni", placeholder:="00000000", disabled)
      )
    )
  )

  def carModal = genericModal("Añadir coche", "addCarModal", addForm, "Añadir!", "submitcoche", addCarSubmit)
  def payModal = genericModal("Hacer pago", "payModal", paymentForm, "Hacer pago", "hacerpago", makePayment)

  def resetM(ids: String, rend: TypedTag[org.scalajs.dom.raw.HTMLDivElement], cb: () => Unit = () => {}): Unit = {
    $(s"#$ids").on("hidden.bs.modal", (e: JQueryEventObject) => {
      $(e.target).remove()
      $("body").append(rend.render)
      resetM(ids, rend, cb)
      cb()
    })
  }

  def enableDisableFact() = {
    if($("#crearFactura").is(":checked")) {
      $("#nombreFactura").prop("disabled", false)
      $("#nifFactura").prop("disabled", false)
    } else {
      $("#nombreFactura").prop("disabled", true)
      $("#nifFactura").prop("disabled", true)
    }
  }

  def getMatriculasReservadas(f: () => Unit = () => {}) = $.get("/api/v1/coches/matriculas?carretera=true", success = (x: js.Any) => {
    globalMatriculas = readDynamic[List[String]](x)
    g.$("#payModal").modal("hide")
    $("#payModal").remove()
    $("body").append(payModal.render)
    $("#payModal").remove()
    $("body").append(payModal.render)
    g.$("#cocheForm").selectpicker()
    g.$("#cocheForm").selectpicker("val", "")
    $("#crearFactura").change((x: js.Any) => enableDisableFact())
    resetM("payModal", payModal, () => {
      g.$("#cocheForm").selectpicker()
      g.$("#cocheForm").selectpicker("val", "")
      $("#crearFactura").change((x: js.Any) => enableDisableFact())
    })
  })

  @JSExport
  def main(fid: Int) = {
    getMatriculasReservadas()

    $("body").append(carModal.render)
    $("body").append(payModal.render)

    $("#content").append(header.render)
    $("#content").append(innerContent.render)

    $("#filtrarText").keyup(hideByText _)

    // Focus first input
    $(dom.document).on("shown.bs.modal", (e: JQueryEventObject) => {
      $(e.target).find("input").first().focus()
    })
    // Reset car modal
    resetM("addCarModal", carModal(), () => g.$("#gamaForm").selectpicker())

    val cochesOrder: Order[Coche] = implicitly[Order[String]].contramap[Coche](_.matricula)
    $.getJSON(s"/api/v1/franquicias/$fid/coches", success = (x: js.Any) => {
      val coches = readDynamic[List[Coche]](x)
      globalCoches = coches
      val target = $("#innerContent")

      coches.sorted(cochesOrder.toScalaOrdering) map { car =>
        target.append(generateCocheUI(car).render).children(s"#${car.matricula}")
      }

      coches map { car => {
        $(s"#${car.matricula}Estado").`val`(car.estado: String)
        g.$(s"#${car.matricula}Estado").selectpicker("render")
      } }
    })
  }
}
