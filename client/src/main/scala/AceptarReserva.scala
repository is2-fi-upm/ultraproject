package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import js.Dynamic.{ global => g }
import org.scalajs.jquery.{jQuery=>$,_}
import jsclasses.{Franquicia, Coche}
import shared.classes.{TipoReserva, Estado, Gama}
import TipoReserva._
import Estado._
import Gama._
import example.helpers.Implicits._
import example.helpers.Methods._

@JSExport
object AceptarReserva {
  def ui() = div(`class`:="container",
    div(`class`:="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad", style:="margin-top: 1em;",
      div(`class`:="panel panel-info",
        div(`class`:="panel-heading",
          h3(`class`:="panel-title", "Informacion de Reserva para aceptar")
            // h3(`class`:="panel-title", id:="cocheName", "")
        ),
        div(`class`:="panel-body",
          div(`class`:="row",
            div(`class`:="col-md-3 col-lg-3", "align".attr:="center", img(id:="cocheImage", alt:="", src:="", `class`:="img-thumbnail img-responsive"),
              label(id:="cocheName", "")
            ),
            div(`class`:=" col-md-9 col-lg-9 ",
              table(`class`:="table table-user-information",
                tbody(
                  tr(
                    td("Reserva para:"),
                    td(id:="usuarioField", "")
                  ),
                  tr(
                    td("Reservado por:"),
                    td(id:="reservanteField", "")
                  ),
                  tr(
                    td("Fecha de inicio:"),
                    td(id:="fechainicioField", "")
                  ),
                  tr(
                    td("Tipo de reserva::"),
                    td(id:="tiporeservaField", "")
                  ),
                  tr(
                    td("Precio:"),
                    td(id:="precioField", "")
                  )
                )
              )
            )
          ),
          div(`class`:="row",
            div(style:="text-align: center;",
              div(style:="display: inline-block; width: 100%;",
                a(href:="#", id:="aceptarBtn", `class`:="btn btn-success btn-lg", "Aceptar"),
                a(href:="#", id:="cancelarBtn", `class`:="btn btn-default btn-lg", "Cancelar")
              )
            )
          )
        )
      )
    )
  )

  def truncate(num: Double) = Math.round(num * 100) / 100

  @JSExport
  def main(id: String, idusuario: String, idreservante: String, idcoche: String, fechainicio: String, tiporeserva: String, precio: Double) = {
    $("#content").append(ui().render)
    $.getJSON(s"/api/v1/coches/$idcoche", success = (x: js.Any) => {
      val c = readDynamic[Coche](x)
      $("#cocheName").text(s"${c.marca} ${c.modelo}")
      // $("#cocheImage").attr("src", s"data:image/png;base64, ${c.foto}")
      $("#cocheImage").attr("src", s"/assets/cars/${c.foto}")
    })
    $("#usuarioField").text(idusuario)
    $("#reservanteField").text(idreservante)
    $("#fechainicioField").text(fechainicio)
    $("#tiporeservaField").text(tiporeserva)
    $("#precioField").text(s"${truncate(precio)}€")

    def redirect(x: js.Any) = {
      val url = readDynamic[String](x)
      dom.window.location.href = url
    }

    $("#aceptarBtn").click(() => {
      $.post(s"/aceptarReserva/${id}/aceptar", success = redirect _)
    })
    $("#cancelarBtn").click(() => {
      $.post(s"/aceptarReserva/${id}/cancelar", success = redirect _)
    })

  }
}
