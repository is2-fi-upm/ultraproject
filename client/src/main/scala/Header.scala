package example


// import scalatags.JsDom.tags2._
// import scalatags.JsDom.all._
import scalatags.Text.all._
import scalatags.Text.tags2.nav

import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import org.scalajs.jquery.{jQuery=>$,_}
import shared.classes.TipoUsuario
import TipoUsuario._
import shared.dbclasses.Usuario
import scala.util.Try

@JSExport
object Header {
  @JSExport
  def main(user: String, tipoStr: String) = {
    val tipo: TipoUsuario = tipoStr
    val needToLogin = li(`class`:="dropdown",
      a(href:="#", `class`:="dropdown-toggle", data("toggle"):="dropdown",
        b("Iniciar Sesion"), span(`class`:="caret")),
      ul(id:="login-dp", `class`:="dropdown-menu",
        li(
          div(`class`:="row",

            div(`class`:="col-md-12",
              form(`class`:="form", role:="form", method:="POST", action:="/login", "accept-charset".attr:="UTF-8", id:="login-nav",
                div(`class`:="form-group",
                  label(`class`:="sr-only", `for`:="exampleDNI", "DNI"),
                  input(`type`:="text", `class`:="form-control", id:="exampleDNI", placeholder:="DNI", required, name:="dni")
                ),
                div(`class`:="form-group",
                  label(`class`:="sr-only", `for`:="examplePassword", "Password"),
                  input(`type`:="password", `class`:="form-control", id:="examplePassword", placeholder:="Password", required, name:="password")
                ),
                a(rel:="login", href:="/login", title:="Crear cuenta",
                  style:="float: right; margin-bottom: 4px;",
                  b("Crear cuenta")),
                div(`class`:="form-group",
                  button(`type`:="submit", `class`:="btn btn-primary btn-block", "Entrar")
                )
              )
            )
          )
        )
      )
    )

    def userLogged(user: String, extra: scalatags.Text.TypedTag[String]) = {
      li(`class`:="dropdown",
        a(href:="#", `class`:="dropdown-toggle", data("toggle"):="dropdown",
          user, span(`class`:="glyphicon glyphicon-user pull-right", style:="margin-left: 8px; padding-top: 3px;")),
        ul(id:="user-dp", `class`:="dropdown-menu", style:="margin-top:8px;",
          li(a(rel:="profile", href:="/profile", title:="Profile", b("Profile"),
            span(`class`:="glyphicon glyphicon-cog pull-right", style:="margin-top: -16px;")
          )),
          extra,
          li(
            a(rel:="logout", href:="/logout", title:="Logout", b("Logout"),
              span(`class`:="glyphicon glyphicon-log-out pull-right", style:="margin-top: -16px;")
            )
          )
        )
      )
    }

    val extraMenu = tipo match {
      case Empleado => li(a(rel:="gestionar", href:="/gestionar", title:="Gestion", b("Gestion"),
        span(`class`:="glyphicon glyphicon-cog pull-right", style:="margin-top: -16px;")
      ))
      case Admin => li(a(rel:="admin", href:="/admin", title:="Admin", b("Admin"),
        span(`class`:="glyphicon glyphicon-cog pull-right", style:="margin-top: -16px;")
      ))
      case _ => li()
    }

    val userHTML = user match {
      case "" => needToLogin
      case user => userLogged(user, extraMenu)
    }

    val headerHTML =
      nav(`class`:="navbar navbar-default navbar-fixed-top",
        div(`class`:="container-fluid",
          div(`class`:="navbar-header",
            button(`type`:="button", `class`:="navbar-toggle collapsed", data("toggle"):="collapse", data("target"):="#bs-example-navbar-collapse-1", "aria-expanded".attr:="false",
              span(`class`:="sr-only", "Toggle navigation"),
              span(`class`:="icon-bar"),
              span(`class`:="icon-bar"),
              span(`class`:="icon-bar")
            ),
            a(`class`:="navbar-brand", rel:="home", href:="/", title:="Shut Up and Rent My Car",
              img(style:="max-width:87px; margin-top: -15px; margin-left: -14px;", src:="/assets/images/fry.png")),
            if($("body").width() >= 490) img(style:="", src:="/assets/images/logo2.png")
          ),
          div(`class`:="collapse navbar-collapse", id:="bs-example-navbar-collapse-1",
            // ul(`class`:="nav navbar-nav",
            //   li(`class`:="", a(href:="#", "Link"))
            // ),
            ul(`class`:="nav navbar-nav navbar-right",
              userHTML
            )
          )
        )
      )

    $("#header").append(headerHTML.render)
  }

}
