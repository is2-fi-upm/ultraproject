package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import org.scalajs.jquery.{jQuery=>$,_}

@JSExport
object Index {

  def welcome(user: String) = h1(
    s"Welcome $user!"
  )

  @JSExport
  def main(user: String) = {
    $(".content").append(welcome(if (user == "") "Anonymous" else user).render)
  }
}
