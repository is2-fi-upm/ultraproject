package jsclasses

case class Franquicia(
  id: Int,
  calle: String,
  numero: Int,
  codigopostal: Int,
  poblacion: String,
  comunidad: String,
  pais: String)
