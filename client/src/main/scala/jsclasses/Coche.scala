package jsclasses

import shared.classes.{Gama, Estado}

case class Coche(
  matricula: String,
  marca: String,
  modelo: String,
  gama: Gama,
  foto: String,
  estado: Estado,
  reserva: Int
)
