package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import org.scalajs.dom._
import org.scalajs.jquery.{jQuery=>$,_}
import scala.concurrent.duration._
import monifu.concurrent.Implicits.globalScheduler
import scala.language.implicitConversions
import scala.language.postfixOps
import monifu.reactive._
import monifu.reactive.channels.PublishChannel

@JSExport
object RxExTimer {
  implicit def int2String(s: Int): String = if (s >= 10) s.toString else s"0${s.toString}"
  implicit def long2String(s: Long): String = if (s >= 10) s.toString else s"0${s.toString}"

  def hours = $("#hours")
  def mins = $("#mins")
  def secs = $("#secs")

  def ui() {
    val timeui = div(
      div(id:="hours", styles.float:="left", "00"),
      div(styles.float:="left", ":"),
      div(id:="mins", styles.float:="left", "00"),
      div(styles.float:="left", ":"),
      div(id:="secs", styles.float:="left", "00")
    )

    $(".content").append(timeui.render)
  }

  def bemorereactive() {
    val maxs = 60
    val maxm = maxs
    val maxh = maxs
    val modh = maxs * maxh

    val timer = Observable.interval(1 second)
    timer foreach { t => secs.text(t % maxs) }

    timer filter { _ % maxs == 0 } foreach { t =>
      mins.text((t / maxs) % maxm)
    }

    timer filter { _ % modh == 0 } foreach { t =>
      hours.text((t / modh) % maxh)
    }
  }

  def mouse() {
    val mousemoves = PublishChannel[(Double, Double)](OverflowStrategy.Unbounded)
    dom.onmousemove = (m: MouseEvent) =>  mousemoves.pushNext((m.clientX, m.clientY))
    mousemoves foreach { case (x,y) => println(x,y) }
  }

  @JSExport
  def main() {
    ui()
    bemorereactive()
    mouse()
  }
}
