package example.helpers

import scala.scalajs.js

object Methods {
  def readDynamic[T: upickle.default.Reader](v: js.Any): T = upickle.default.read[T](js.JSON.stringify(v))
}
