package example.helpers

import shared.classes._
import Gama._
import Estado._
import TipoUsuario._
import TipoReserva._
// import java.sql.Blob
// import java.util.Base64
// import javax.sql.rowset.serial.SerialBlob
import upickle.Js

object Implicits {
  def toReader[T]()(implicit f: String => T) = upickle.default.Reader[T] {
    case Js.Str(str) => f(str)
  }
  def toWriter[T]()(implicit f: T => String) = upickle.default.Writer[T] {
    case g => Js.Str(f(g))
  }

  implicit val gama2Reader = toReader[Gama]()
  implicit val gama2Writer = toWriter[Gama]()

  implicit val estado2Reader = toReader[Estado]()
  implicit val estado2Writer = toWriter[Estado]()

  implicit val tipousuario2Reader = toReader[TipoUsuario]()
  implicit val tipousuario2Writer = toWriter[TipoUsuario]()

  implicit val tiporeserva2Reader = toReader[TipoReserva]()
  implicit val tiporeserva2Writer = toWriter[TipoReserva]()

  // implicit def blob2String(b: Blob): String = Base64.getEncoder.encodeToString(b.getBytes(1, b.length.toInt))
  // implicit def string2Blob(str: String): Blob = new SerialBlob(Base64.getDecoder.decode(str))
  // implicit val blob2Reader = toReader[Blob]()
  // implicit val blob2Writer = toWriter[Blob]()
}
