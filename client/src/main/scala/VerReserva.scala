package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import js.Dynamic.{ global => g }
import org.scalajs.jquery.{jQuery=>$,_}
import jsclasses.{Franquicia, Coche}
import shared.classes.{TipoReserva, Estado, Gama}
import TipoReserva._
import Estado._
import Gama._
import example.helpers.Implicits._
import example.helpers.Methods._

@JSExport
object VerReserva {
  def ui() = div(`class`:="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad", style:="margin-top: 1em;",
      div(`class`:="panel panel-info",
        div(`class`:="panel-heading",
          h3(`class`:="panel-title", "Reserva")
            // h3(`class`:="panel-title", id:="cocheName", "")
        ),
        div(`class`:="panel-body",
          div(`class`:="row",
            div(`class`:="col-md-3 col-lg-3", "align".attr:="center", img(id:="cocheImage", alt:="", src:="", `class`:="img-thumbnail img-responsive"),
              label(id:="cocheName", "")
            ),
            div(`class`:=" col-md-9 col-lg-9 ",
              table(`class`:="table table-user-information",
                tbody(
                  tr(
                    td("Reserva para:"),
                    td(id:="usuarioField", "")
                  ),
                  tr(
                    td("Reservado por:"),
                    td(id:="reservanteField", "")
                  ),
                  tr(
                    td("Tarjeta:"),
                    td(id:="tarjetaField", "")
                  ),
                  tr(
                    td("Fecha de inicio:"),
                    td(id:="fechainicioField", "")
                  ),
                  tr(
                    td("Tipo de reserva::"),
                    td(id:="tiporeservaField", "")
                  ),
                  tr(
                    td("Precio:"),
                    td(id:="precioField", "")
                  )
                )
              )
            )
          )
        )
      )
    )

  def pDate(d: js.Date) = s"${d.getDate()}/${d.getMonth()}/${d.getFullYear()}"

  def truncate(num: Double) = Math.round(num * 100) / 100

  @JSExport
  def main(id: Int, usuario: String, reservante: String, coche: String, tarjeta: String, fechainic: String, tipo: String, precio: Double) = {
    $("#content").append(ui().render)
    $.getJSON(s"/api/v1/coches/$coche", success = (x: js.Any) => {
      val c = readDynamic[Coche](x)
      $("#cocheName").text(s"${c.marca} ${c.modelo}")
      // $("#cocheImage").attr("src", s"data:image/png;base64, ${c.foto}")
      $("#cocheImage").attr("src", s"/assets/cars/${c.foto}")
    })
    $("#usuarioField").text(usuario)
    $("#reservanteField").text(reservante)
    $("#tarjetaField").text(tarjeta)
    $("#fechainicioField").text(fechainic)
    $("#tiporeservaField").text(tipo)
    $("#precioField").text(s"${truncate(precio)}€")
  }
}
