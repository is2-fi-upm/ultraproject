package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import js.Dynamic.{ global => g }
import org.scalajs.jquery.{ jQuery=>$, _}
import upickle.Js
import upickle.{default => upickle}
import jsclasses.{Franquicia, Coche}
import shared.classes.Estado
import Estado._
import example.helpers.Implicits._
import example.helpers.Methods._
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue
import scala.concurrent.Future

@JSExport
object ShowCochesFranquicia {
  var franquiciaId = -1
  var globalCoches: List[Coche] = List()

  def ui() = {
    div(
      div(id:="info", style:="text-align: center;"),
      ul(`class`:="thumbnails col-md-12 list-unstyled",
        id:="cochesContainer",
        style:="height: 100%; display: flex; flex-flow: row wrap; align-items: center; justify-content: center;")
    )
  }

  def reservarCoche(id: String)() = {
    if (franquiciaId > 0)
      g.window.location.href = s"/reservar/$franquiciaId/$id"
  }

  def generateCocheUI(c: Coche) = {
    val dispo = c.estado.isInstanceOf[Disponible.type]
    li(`class`:="col-md-3 cocheTarjeta",
      id:=c.matricula,
      if (!dispo) disabled,
      div(`class`:="thumbnail", style:="padding: 0; height: 100%;",
        div(style:="padding: 4px;",
          img(alt:=c.matricula, style:="width: 100%;", height:="150px", // width:="200px",
                                                                        // src:=s"data:image/png;base64, ${c.foto}"
            src:=s"/assets/cars/${c.foto}"
          )
        ),
        div(`class`:="caption", // style:="text-align: left;",
          h2(s"${c.marca} ${c.modelo}"),
          p(s"Gama ${c.gama}"),
          p(i(`class`:="icon icon-map-marker"), s"${c.estado}")
        ),
        div(`class`:="modal-footer", style:="text-align: center;",
          button(
            onclick:=reservarCoche(c.matricula) _,
            `class`:=s"btn ${if(dispo) "btn-success" else "btn-danger"} btn-lg",
            if (!dispo) disabled,
            if (dispo) "Reservar" else "No disponible"
          ))
      )
    )
  }

  def createSelector(name: String, x: List[Tuple2[String, String]], f: String => Unit) = {
    select(
      id:=name,
      `class`:="selectpicker",
      "data-live-search".attr:="true",
      onchange:= {() => {
        val selected = $(s"#$name").`val`().asInstanceOf[String]
        f(selected)
      }},
      x map { case (y, z) => option(value:=y, z) }
    )
  }

  def equalGamaOrDefault(g: String, g2: String) = g.toLowerCase == "gama" || g.toLowerCase == g2.toLowerCase

  def hideByGama(g: String) = {
    globalCoches map { c =>
      val selector = $(s"#${c.matricula}")
      equalGamaOrDefault(g, c.gama) match {
        case false => selector.fadeOut(50)
        case true => selector.fadeIn(50)
      }
    }
  }

  def hideByText(e: JQueryEventObject): js.Any = {
    def carText(c: Coche) = s"${c.matricula}${c.marca}${c.modelo}${c.gama}${c.estado}"
    val gamaSelected = $("#selectGama").`val`().asInstanceOf[String]
    val text = $("#filtrarText").`val`().asInstanceOf[String]
    text.replaceAll(" ", "").toLowerCase() match {
      case "" => hideByGama(gamaSelected)
      case t => {
        globalCoches map {car =>
          if (carText(car).replaceAll(" ", "").toLowerCase().contains(t) && equalGamaOrDefault(gamaSelected, car.gama))
            $(s"#${car.matricula}").fadeIn(50)
          else
            $(s"#${car.matricula}").fadeOut(50)
        }
      }
    }
  }

  @JSExport
  def main(id: Int, calle: String, numero: Int, pais: String, comunidad: String, poblacion: String, codigopostal: Int) = {
    franquiciaId = id
    $("#content").append(ui().render)
    $("#info").append(
      div(style:="width: 50%;margin: 0 auto;",
        h3(s"$calle - $numero (CP: $codigopostal)"),
        div(`class`:="login-top-row", style:="width: 100%",
          div(`class`:="form-group", style:="margin-right: 0px;",
            createSelector("selectGama", List(("gama", "Gama"), ("Alta", "Alta"), ("Media", "Media"), ("Baja", "Baja")), hideByGama)),
          div(`class`:="form-group",
            input(`class`:="form-control login-input", "id".attr:="filtrarText",
              placeholder:="Filtrar por texto", style:="color: black;")
          )

        )
      ).render
    )

    g.window.setTimeout(() => {
      $(".btn.dropdown-toggle.btn-default").css("width", "70%")
    }, 100)

    $("#filtrarText").keyup(hideByText _)

    $("#cochesContainer").empty().append(img(src:="/assets/images/loadCoches.gif").render)
    $.getJSON(s"/api/v1/franquicias/$id/coches", success = (x: js.Any) => {

      val coches = readDynamic[List[Coche]](x) filter { !_.estado.isInstanceOf[Baja.type] }
      globalCoches = coches
      // val cochesDivs = if (coches.length > 0) (coches map generateCocheUI)
      // else List(h1("No hay coches en esta franquicia ahora mismo..."))

      g.window.setTimeout(() => {
        val target = $("#cochesContainer")
        target.children().fadeOut(300)

        g.window.setTimeout(() => {
          // var maxHeight = 0
          if (coches.length > 0) {
            coches map { car => {
              target.append(generateCocheUI(car).render)
              // maxHeight = js.Math.max($(s"#${car.matricula}").height().toInt, maxHeight)
              $(s"#${car.matricula} img").width($(s"#${car.matricula}").width() - 10)
            }}

            // coches map { car =>
            //   $(s"#${car.matricula}").height(maxHeight.toDouble)
            // }

          }
          else target.append(h1("No hay coches en esta franquicia ahora mismo...").render)
        }, 300)

        // val rendered = cochesDivs.render
        // target.append(rendered).fadeIn(300)
      }, 2200)
    })
  }
}
