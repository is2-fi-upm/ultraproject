package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import js.Dynamic.{ global => g }
import org.scalajs.jquery.{ jQuery=>$ }
import upickle.Js
import upickle.{default => upickle}
import jsclasses.{Franquicia, Coche}
import shared.classes.Estado
import Estado._
import example.helpers.Implicits._
import example.helpers.Methods._

@JSExport
object NewMain {
  val emptyElem = "Select one"
  val disSel = select(`class`:="selectpicker disabled-select", disabled, emptyElem)
  var franquicias: Option[List[Franquicia]] = None

  def center(outer: js.Any, content: String) = {
    $(content).css("margin-top", ($(outer).height() - $(content).outerHeight()) / 4)
  }

  def createSelector(name: String, x: List[Tuple2[String, String]], f: String => Unit) = {
    select(
      id:=name,
      `class`:="selectpicker",
      "data-live-search".attr:="true",
      onchange:= {() => {
        val selected = $(s"#$name").`val`().asInstanceOf[String]
        f(selected)
      }},
      x map { case (y, z) => option(value:=y, z) }
    )
  }

  def ui() = {
    div(id:="franquiciaContainer",
      h1(`class`:="text-center", "Selecciona tu franquicia mas cercana"),
      div(
        id:="selectorContainer", style:="margin-top: 20px;",
        div(`class`:="form-group", label("Pais"), div(id:="paisdiv", disSel)),
        div(`class`:="form-group", label("Comunidad"), div(id:="comunidaddiv", disSel)),
        div(`class`:="form-group", label("Poblacion"), div(id:="poblaciondiv", disSel)),
        div(`class`:="form-group", label("Franquicia"), div(id:="franquiciadiv", disSel)),
        button(id:="sendButton", `class`:="btn btn-default btn-lg", `type`:="button", onclick:=submitData _, disabled, "Send")
      )
    )
  }

  def submitData() = {
    val pais = $("#paisesSelector").`val`().asInstanceOf[String]
    val comunidad = $("#comunidadesSelector").`val`().asInstanceOf[String]
    val poblacion = $("#poblacionesSelector").`val`().asInstanceOf[String]
    val franquicia = $("#franquiciaSelector").`val`().asInstanceOf[String]
    val equalEmpty = List(pais, comunidad, poblacion, franquicia) filter (_ == emptyElem)
    if ( equalEmpty.length > 0) {
      $("#comunidaddiv").empty().append(disSel.render)
      $("#poblaciondiv").empty().append(disSel.render)
      $("#franquiciadiv").empty().append(disSel.render)
      setButton(true)
    } else {
      g.window.location.href = s"/vercoches/$franquicia"
    }
  }

  def setButton(p: Boolean) = {
    $("#sendButton").prop("disabled", p);
    if (!p) {
      $("#sendButton").removeClass("btn-default")
      $("#sendButton").addClass("btn-success")
    } else {
      $("#sendButton").addClass("btn-default")
      $("#sendButton").removeClass("btn-success")
    }
  }

  // def generateCocheUI(c: Coche) = {
  //   div(
  //     id:=c.matricula,
  //     if (!c.estado.isInstanceOf[Disponible.type]) disabled,
  //     `class`:="coche",
  //     h3(s"${c.marca} ${c.modelo} - Gama ${c.gama} - ${c.estado}"),
  //     img(
  //       src:=s"data:image/png;base64, ${c.foto}"
  //     )
  //   )
  // }

  // def getCochesFranquicia(id: String) = {
  //   $("#cochesContainer").empty().append(img(src:="/assets/images/loadCoches.gif").render)
  //   $.getJSON(s"/api/v1/franquicias/$id/coches", success = (x: js.Any) => {
  //     val coches = readDynamic[List[Coche]](x) filter { !_.estado.isInstanceOf[Baja.type] }
  //     // if coches.length <= 0 :'(
  //     val cochesDivs = coches map generateCocheUI
  //     $("#cochesContainer").empty().append(cochesDivs.render)
  //   })
  // }

  def franquiciaToString(f: Franquicia) = s"${f.calle} - ${f.numero} (CP: ${f.codigopostal})"

  def selectPoblacion(pobl: String) = {
    resetSelectors(false, false)
    setButton(true)
    if (pobl != emptyElem) {
      val p = $("#paisesSelector").`val`().asInstanceOf[String]
      val c = $("#comunidadesSelector").`val`().asInstanceOf[String]

      $.getJSON(s"/api/v1/franquicias/filter?pais=$p&comunidad=$c&poblacion=$pobl", success = (x: js.Any) => {
        val franqs = readDynamic[List[Franquicia]](x)
        franquicias = Some(franqs)
        val franqsList = (franqs map { _.id.toString }) zip (franqs map franquiciaToString)
        val lista = List((emptyElem, emptyElem)) ++ franqsList
        // val franqsel = createSelector("franquiciaSelector", lista, getCochesFranquicia)
        val franqsel = createSelector("franquiciaSelector", lista, {(f: String) => {
          setButton(f == emptyElem)
          $("#sendButton").focus()
        }})
        $("#franquiciadiv").empty().append(franqsel.render)
        selectAndFocus("franquiciaSelector")
      })
    }
  }

  def selectComunidad(c: String) = {
    resetSelectors(false)
    setButton(true)
    if (c != emptyElem) {
      val p = $("#paisesSelector").`val`().asInstanceOf[String]
      $.getJSON(s"/api/v1/franquicias/filter?pais=$p&comunidad=$c", success = (x: js.Any) => {
        val poblaciones = readDynamic[List[String]](x)
        val lista = (List(emptyElem) ++ poblaciones) map { x => (x, x)}
        val poblsel = createSelector("poblacionesSelector", lista, selectPoblacion)
        $("#poblaciondiv").empty().append(poblsel.render)
        selectAndFocus("poblacionesSelector")
      })
    }
  }

  def selectCountry(p: String) = {
    resetSelectors()
    setButton(true)
    if (p != emptyElem) {
      $.getJSON(s"/api/v1/franquicias/filter?pais=$p", success = (x: js.Any) => {
        val comunidades = readDynamic[List[String]](x)
        val lista = (List(emptyElem) ++ comunidades) map { x => (x, x)}
        val comsel = createSelector("comunidadesSelector", lista, selectComunidad)
        $("#comunidaddiv").empty().append(comsel.render)
        selectAndFocus("comunidadesSelector")
      })
    }
  }

  def selectAndFocus(name: String) = {
    g.$(s"#$name").selectpicker()
    $(s"#$name").next("div").children().focus()
  }

  def resetSelectors(com: Boolean = true, pobl: Boolean = true, franq: Boolean = true) = {
    if (com) $("#comunidaddiv").empty().append(disSel.render)
    if (pobl) $("#poblaciondiv").empty().append(disSel.render)
    if (franq) $("#franquiciadiv").empty().append(disSel.render)
    g.$(".disabled-select").selectpicker()
  }

  @JSExport
  def main() = {
    $("#content").append(ui().render)
    $.getJSON("/api/v1/franquicias/filter", success = (x: js.Any) => {
      val countries = readDynamic[List[String]](x)
      val lista = (List(emptyElem) ++ countries) map { x => (x, x)}
      val countrsel = createSelector("paisesSelector", lista, selectCountry)

      $("#paisdiv").empty().append(countrsel.render)
      selectAndFocus("paisesSelector")
    })

    // $(g.window).resize(() => {
    //   center("#franquiciaContainer", "#selectorContainer")
    // })
  }
}
