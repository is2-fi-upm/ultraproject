package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import js.Dynamic.{ global => g }
import org.scalajs.jquery.{jQuery=>$,_}
import example.helpers.Methods._
// import shared.classes.Gama
// import Gama._

@JSExport
object Reserva {
  def center(outer: js.Any, content: String) = {
    if ($(outer).width() >= 600) {
      $(content).css("width", "600")
    }
    $(content).css("margin-left", ($(outer).width() - $(content).outerWidth())/2)
    $(content).css("margin-top", 15)
  }


  def createField(lbl: String,
    fieldname: String,
    typ: String = "text",
    readon: String = "") = div(`class`:="login-field-wrap",
      label(`class`:=s"${if(readon == "") "login-label" else "login-label active highlight"}", lbl, span(`class`:="req", "*")),
      input(`class`:="login-input", name:=fieldname, `type`:=typ, required, autocomplete:="on", if(readon != "") readonly, if(readon != "") value:=readon)
  )

  def createSelector(nameid: String, x: List[Tuple2[String, String]], f: String => Unit) = {
    select(
      id:=nameid,
      name:=nameid,
      `class`:="selectpicker",
      "data-live-search".attr:="true",
      onchange:= {() => {
        val selected = $(s"#$nameid").`val`().asInstanceOf[String]
        f(selected)
      }},
      option(`class`:="active selected", value:=x(0)._1, x(0)._2),
      x.tail map { case (y, z) => option(value:=y, z) }
    )
  }

  def updatePrice(v: String) = {
    val coche = $("#idcoche").`val`().asInstanceOf[String]
    val fechaOriginal = $("#fechainicio").`val`().asInstanceOf[String]
    val fecha = fechaOriginal.split("/").reverse.mkString("-")
    val reserva = $("#tiporeserva").`val`().asInstanceOf[String]
    $.getJSON(s"/api/v1/precio?coche=${coche}&fecha=${fecha}&reserva=${reserva}", success = (x: js.Any) => {
      val price = readDynamic[Double](x)
      val priceText = if(reserva == "km")
        s"${price}€ por KM"
      else
        s"${price}€"
      $("#price").text(priceText)
    })
  }

  def createDatepicker(fieldname: String, format: String = "mm/dd/yyyy") = input(
    `class`:="datepicker",
    id:=fieldname,
    name:=fieldname,
    "data-date-format".attr:=format)

  @JSExport
  def main(coche: String,
    gamaCocheStr: String,
    franq: Int,
    userid: String,
    both: Boolean,
    errortext: String = "",
    successtext: String = "") = {

    val tiposreservas = List(("dia", "Un dia"), ("semana", "Una semana"), ("finsemana", "Un fin de semana"), ("km", "Precio por kilometro"))

    val tipost1 = List("Mastercard", "Visa", "American Express")
    val tipostarjetas = tipost1 zip tipost1

    val today = new js.Date()

    val reservaHTML = div(id:="selectorContainer",
      form(action:=s"/reservar/$franq/$coche", method:="POST",
        div(`class`:="form-group", label("DNI usuario a reservar"),
          input(`class`:="form-control login-input", id:="idusuario",
            name:="idusuario", if (both) value:=userid, if (both) readonly)),
        input(id:="idcoche", name:="idcoche", `type`:="hidden", required, value:=coche),
        div(`class`:="form-group", label("Numero de tarjeta de credito"),
          input(`class`:="form-control login-input", id:="tarjeta",
            name:="tarjeta", placeholder:="4707517714551133")),

        div(`class`:="login-top-row",
          div(`class`:="form-group", label("Caducidad de la tarjeta"),
          div(`class`:="input-group date",
            input(`type`:="text", `class`:="form-control login-input", value:=s"${today.getMonth()}/${today.getFullYear().toString().substring(2)}", placeholder:=s"${today.getMonth()}/${today.getFullYear().toString().substring(2)}", id:="caducidadtarjeta", name:="caducidadtarjeta",
              span(`class`:="input-group-addon", i(`class`:="glyphicon glyphicon-th"))))),
        div(`class`:="form-group", label("Tipo de tarjeta"), div(id:="tipodeTarjedaDiv", createSelector("tipotarjeta", tipostarjetas, _ => {})))
        ),

        div(`class`:="login-top-row",
          div(`class`:="form-group", label("Fecha de inicio de reserva"),
            div(`class`:="input-group date",
              input(`type`:="text", `class`:="form-control login-input", value:=s"${today.getDate()}/${today.getMonth()}/${today.getFullYear()}", placeholder:=s"${today.getDay()}/${today.getMonth()}/${today.getFullYear()}", id:="fechainicio", name:="fechainicio", span(`class`:="input-group-addon", i(`class`:="glyphicon glyphicon-th"))))),
          div(`class`:="form-group", label("Tipo de reserva"), div(id:="tiporeservaDiv", createSelector("tiporeserva", tiposreservas, updatePrice _)))
        ),

        div(style:="text-align: center; margin-top: -10px;",
          div(style:="display: inline-block; width: 100%;",
            label(style:="margin-top:-10px;","Precio calculado"),
            br(),
            label(style:="font-size: 4em; margin-top: -0.3em;", id:="price", "You text")
          )),

        // label(style:="margin-top:-10px;", "Precio calculado: ",
        //   // br(),
        //   div(style:="width: 200px;display: block;margin-left: auto;margin-right: auto;",
        //     "TEXT",
        //     label(style:="font-size: 31px;", id:="price", "0€"))),
        button(`type`:="sumbit", `class`:="btn btn-success", "Reservar!")
      )
    )

    $("#content").append(reservaHTML.render)

    g.$("#fechainicio").datepicker(js.Dynamic.literal(
      "format" -> "dd/mm/yyyy",
      "startDate" -> "-0days",
      "todayBtn" -> "linked",
      "language" -> "es",
      "autoclose" -> true,
      "todayHighlight" -> true
    ))

    g.$("#caducidadtarjeta").datepicker(js.Dynamic.literal(
      "format" -> "mm/yy",
      "startDate" -> "-0days",
      "startView" -> 2,
      "minViewMode" -> 1,
      "clearBtn" -> true,
      "language" -> "es",
      "calendarWeeks" -> true,
      "autoclose" -> true))

    updatePrice("")

    $("#selectorContainer").find("input").on("keyup blur focus", (e: JQueryEventObject) => {
      val $this = $(e.target)
      val label = $this.prev("label")
      val isempty = $this.`val`().asInstanceOf[String] == ""
      val classes = e.`type` match {
        case "keyup" => if (isempty) "active highlight" else "active highlight"
        case "blur" => if (isempty) "active highlight" else "highlight"
        case "focus" => if (isempty) "highlight" else "highlight"
        case _ => ""
      }
      if (isempty)
        label.removeClass(classes)
      else
        label.addClass(classes)
    })

    center("#content", "#selectorContainer")
    $(g.window).resize(() => {
      center("#content", "#selectorContainer")
    })
  }
}
