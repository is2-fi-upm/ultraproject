package example

import scala.language.dynamics
import scalatags.JsDom._
import all._
import scala.scalajs.js.annotation.JSExport
import scala.concurrent.ExecutionContext.Implicits.global
import jsclasses.Coche
import org.scalajs.dom._
import org.scalajs.jquery.{jQuery=>$,_}
import upickle.Js
import upickle.{default => upickle}
import scala.scalajs.js
import example.helpers.Implicits._
import example.helpers.Methods._

@JSExport
object Coches {

  @JSExport
  def main() = {
    // ext.Ajax.get(
    //   url="/api/v1/testcoches"
    // ).map(x => {
    //   console.log(x.response)
    // })

    $.getJSON("/api/v1/testcoches", success = (x: js.Any) => {
      val xl = readDynamic[List[Coche]](x)
      xl foreach { y => {
        println(y.matricula)
      }}
    }

    // $.getJSON("/api/v1/testcoches", success = (x: js.Array[js.Dynamic]) => {
    //   val xl = readDynamic[List[Coche]](x)
    //   xl foreach println
    //   println(x.length)
    //   x map (y => {
    //     val yc = readDynamic[Coche](y)
    //     println(yc.matricula)
    //   })
    // }

    )
  }
}
