package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import org.scalajs.jquery.{jQuery=>$,_}
import scala.concurrent.duration._
import rx._
import rx.ops._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.language.implicitConversions
import scala.language.postfixOps

@JSExport
object RxTimer {
  implicit val scheduler = new DomScheduler
  implicit def int2String(s: Int): String = if (s >= 10) s.toString else s"0${s.toString}"
  implicit def long2String(s: Long): String = if (s >= 10) s.toString else s"0${s.toString}"

  def hours = $("#hours")
  def mins = $("#mins")
  def secs = $("#secs")

  def ui() {
    val timeui = div(
      div(id:="hours", styles.float:="left", "00"),
      div(styles.float:="left", ":"),
      div(id:="mins", styles.float:="left", "00"),
      div(styles.float:="left", ":"),
      div(id:="secs", styles.float:="left", "00")
    )

    $(".content").append(timeui.render)
  }

  def bemorereactive() {
    val maxs = 60

    val h = Var(0)
    val m = Var(0)
    val s = Var(0)

    h foreach { nh => hours.text(nh) }

    m foreach { nm =>
      if (nm >= maxs) {
        m() = 0
        h() = h() + 1
      } else {
        mins.text(nm)
      }
    }

    s foreach { ns =>
      if (ns >= maxs) {
        s() = 0
        m() = m() + 1
      } else {
        secs.text(ns)
      }
    }

    val t = Timer(100 millis, 1 second)
    t foreach { x => s() = s() + 1 }
  }

  def bereactive() {
    val maxs = 60

    val s = Var(0.toLong)
    val m = Var(0)
    val h = Var(0)

    val ss = Rx { s() }
    val mm = Rx { m() + (ss() / maxs) }
    val hh = Rx { h() + (mm() / maxs) }

    ss foreach { ns => secs.text(ns % maxs) }
    mm foreach { nm => mins.text(nm % maxs) }
    hh foreach { nh => hours.text(nh % maxs) }

    val t = Timer(500 millis, 1 second)
    t foreach { x => s() = x }
  }

  @JSExport
  def main() {
    ui()
    bemorereactive()
  }
}
