package example

import scalatags.JsDom._
import all._
import scala.scalajs.js
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom
import org.scalajs.jquery.{jQuery=>$,_}

@JSExport
object Saved {

  def showUser(user: String) = h1(
    s"User $user saved ^^"
  )

  @JSExport
  def main(user: String) = {
    $(".content").append(showUser(user).render)
  }
}
