import sbt._
import sbt.Keys._
import org.scalajs.sbtplugin.ScalaJSPlugin.autoImport._

object Settings {
  val name = "is2"
  val version = "0.1.0"

  object versions {
    val scala = "2.11.7"
    val webjars = "2.4.0-1"
    val bootstrap = "0.4.4-P24"
    val fontawesome = "4.3.0-2"
    val datepicker = "1.4.0"
    val scalajsdom = "0.8.0"
    val scalajsjquery = "0.8.1"


    val jQuery = "1.11.1"
    val scalajs = "0.8.0"
    val scalarx = "0.2.8"
    val scalatags = "0.5.2"
    val scalajsReact = "0.9.0"
    // val akka = "2.3.12"
    // val akkaHttp = "1.0"
    // val logBack = "1.1.2"
    val autowire = "0.2.5"
    val upickle = "0.3.4"
    val react = "0.12.2"
  }

  val jsScriptDependencies = Def.setting(Seq(
    "org.webjars" % "jquery" % versions.jQuery / "jquery.js" minified "jquery.min.js",
    "org.webjars" % "bootstrap" % versions.bootstrap / "bootstrap.js" minified "bootstrap.min.js" dependsOn "jquery.js",
    "org.webjars" % "react" % versions.react / "react-with-addons.js" minified "react-with-addons.min.js" commonJSName "React"
  ))

  val jvmDependencies = Def.setting(Seq(
    "org.webjars" %% "webjars-play" % versions.webjars
  ))

}
