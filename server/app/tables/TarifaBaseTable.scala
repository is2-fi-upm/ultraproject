package tables

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import shared.classes.{Gama, TipoReserva}
import TipoReserva._
import Gama._
import aux.Methods._
import shared.dbclasses.TarifaBase
import scala.util.{Try, Success, Failure}

@Singleton()
class TarifaBaseDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  implicit val gamaColumnType = MappedColumnType.base[Gama, String](applyImpl, applyImpl)

  class TarifasBase(tag: Tag) extends Table[TarifaBase](tag, "TARIFABASE") {
    def gama = column[Gama]("GAMA", O.PrimaryKey)
    def precioKm = column[Float]("PRECIOKM")
    def precioDia = column[Float]("PRECIODIA")
    def precioSemana = column[Float]("PRECIOSEMANA")
    def precioFinSemana = column[Float]("PRECIOFINSEMANA")

    def * = (
      gama,
      precioKm,
      precioDia,
      precioSemana,
      precioFinSemana) <> ((TarifaBase.apply _).tupled, TarifaBase.unapply _)
  }

  private val tarifasbase = TableQuery[TarifasBase]

  def list(): Future[Seq[TarifaBase]] = db.run(tarifasbase.result)

  def getByGama(g: Gama): Future[Option[TarifaBase]] = {
    val action = tarifasbase.filter(_.gama === g)
    db.run(action.result) map { _.headOption }
  }

  def getPrecio(g: Gama, t: TipoReserva): Future[Option[Float]] = {
    getByGama(g) map { _ map { tb =>
      t match {
        case Km => tb.precioKm
        case Dia => tb.precioDia
        case Semana => tb.precioSemana
        case FinSemana => tb.precioFinSemana
      }
    }}
  }
}
