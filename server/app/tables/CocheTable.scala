package tables

import java.sql.Blob
import shared.dbclasses.Coche
import shared.classes.{Gama, Estado}
import Gama._
import Estado._
import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import scala.util.{Try, Success}

@Singleton()
class CocheDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  implicit val estadoColumnType = MappedColumnType.base[Estado, String](x => x, y => y)
  implicit val gamaColumnType = MappedColumnType.base[Gama, String](x => x, y => y)

  class Coches(tag: Tag) extends Table[Coche](tag, "COCHE") {
    def matricula = column[String]("MATRICULA", O.PrimaryKey)
    def marca = column[String]("MARCA")
    def modelo = column[String]("MODELO")
    def gama = column[Gama]("GAMA")
    // def foto = column[Blob]("FOTO")
    def foto = column[String]("FOTO")
    def estado = column[Estado]("ESTADO")
    def reserva = column[Int]("RESERVA")

    def * = (
      matricula,
      marca,
      modelo,
      gama,
      foto,
      estado,
      reserva) <> ((Coche.apply _).tupled, Coche.unapply _)
  }

  private val coches = TableQuery[Coches]

  def list(): Future[Seq[Coche]] = {
    db.run(coches.result)
  }

  def listIDs(onlyDisp: Boolean = false, onlyReserv: Boolean = false, onlyCarr: Boolean = false): Future[Seq[String]] = {
    val st: Estado = Disponible
    val res: Estado = Reservado
    val carr: Estado = Carretera
    val action1 = if (onlyDisp) coches.filter(_.estado === st) else coches
    val action2 = if (onlyReserv) action1.filter(_.estado === res) else action1
    val action = if (onlyCarr) action2.filter(_.estado === carr) else action2
    db.run(action.map(_.matricula).result)
  }

  def insert(coche: Coche): Future[Try[Coche]] = {
    val action = coches += coche
    db.run(action.asTry).map(_ => Success(coche))
  }

  def getCoche(matricula: String): Future[Option[Coche]] = {
    val matriculaLowerCase = matricula.toLowerCase
    val action = coches.filter(c => c.matricula.toLowerCase === matriculaLowerCase)
    db.run(action.result) map {
      r => r.headOption
    }
  }

  def getCoches(matriculas: Seq[String]): Future[Seq[Coche]] = {
    val matriculasLowerCase = matriculas map { _.toLowerCase }
    val action = coches
      .filter(c => c.matricula.toLowerCase inSetBind matriculasLowerCase)
    db.run(action.result)
  }

  def changeMultiState(matriculas: Seq[String], newState: Estado) = {
    getCoches(matriculas) map { cs =>
      cs map { c =>
        coches.insertOrUpdate(c.copy(estado=newState))
      } map { quer =>
        Await.result(db.run(quer), 500 milli)
      }
    }
  }

  def changeState(matricula: String, newState: Estado): Future[Int] = {
    getCoche(matricula) flatMap {
      case Some(coche) => {
        db.run(coches.insertOrUpdate(coche.copy(estado=newState)))
      }
      case None => Future.successful(-1)
    }
  }

  def changeReserva(matricula: String, newReserva: Int): Future[Int] = {
    getCoche(matricula) flatMap {
      case Some(coche) => db.run(coches.insertOrUpdate(coche.copy(reserva=newReserva)))
      case None => Future.successful(-1)
    }
  }

  def payCarFranq(matricula: String): Future[Int] = {
    getCoche(matricula) flatMap {
      case Some(coche) => {
        db.run(coches.insertOrUpdate(coche.copy(estado=Disponible, reserva=(-1))))
      }
      case None => Future.successful(-1)
    }
  }
}
