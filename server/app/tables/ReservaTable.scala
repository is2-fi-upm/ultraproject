package tables

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import java.sql.{Timestamp, Date}
import shared.classes.TipoReserva
import TipoReserva._
import aux.Methods._
import shared.dbclasses.Reserva
import scala.util.{Try, Success, Failure}

@Singleton()
class ReservaDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  class Reservas(tag: Tag) extends Table[Reserva](tag, "RESERVA") {
    implicit val tiporeservaColumn = MappedColumnType.base[TipoReserva, String](applyImpl, applyImpl)

    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def idusuario = column[String]("IDUSUARIO")
    def idreservante = column[String]("IDRESERVANTE")
    def idcoche = column[String]("IDCOCHE")
    def tarjeta = column[String]("TARJETA")
    def caducidadtarjeta = column[Date]("CADUCIDADTARJETA")
    def tipotarjeta = column[String]("TIPOTARJETA")
    def fechareserva = column[Timestamp]("FECHARESERVA")
    def fechainicio = column[Date]("FECHAINICIO")
    def tiporeserva = column[TipoReserva]("TIPORESERVA")
    def precio = column[Float]("PRECIO")

    def * = (id.?,
      idusuario,
      idreservante,
      idcoche,
      tarjeta,
      caducidadtarjeta,
      tipotarjeta,
      fechareserva,
      fechainicio,
      tiporeserva,
      precio) <> ((Reserva.apply _).tupled, Reserva.unapply _)
  }

  private val reservas = TableQuery[Reservas]

  def list(): Future[Seq[Reserva]] = db.run(reservas.result)

  def listUsuario(user: String): Future[Seq[Reserva]] = {
    val action = reservas.filter(r => r.idusuario === user || r.idreservante === user)
    db.run(action.result)
  }

  def getReserva(res: Int): Future[Option[Reserva]] = {
    val action = reservas.filter(_.id === res)
    db.run(action.result) map { _.headOption }
  }

  val insertQuery = reservas returning reservas.map(_.id) into ((item, id) => item.copy(id = Some(id)))

  def insert(r: Reserva): Future[Reserva] = {
    val action = insertQuery += r.copy(id = None)
    db.run(action)
  }
}
