package tables

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import java.sql.Timestamp
import shared.classes.Metodo
import Metodo._
import aux.Methods._
import shared.dbclasses.Factura
import scala.util.{Try, Success, Failure}

@Singleton()
class FacturaDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  class Facturas(tag: Tag) extends Table[Factura](tag, "FACTURA") {
    implicit val metodoColumn = MappedColumnType.base[Metodo, String](applyImpl, applyImpl)

    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def nombre = column[String]("NOMBRE")
    def dni = column[String]("DNI")
    def fecha = column[Timestamp]("FECHA")
    def coche = column[String]("COCHE")
    def precio = column[Float]("PRECIO")
    def metodo = column[Metodo]("METODO")
    def franquicia = column[Int]("FRANQUICIA")

    def * = (id.?,
      nombre,
      dni,
      fecha,
      coche,
      precio,
      metodo,
      franquicia) <> ((Factura.apply _).tupled, Factura.unapply _)
  }

  private val facturas = TableQuery[Facturas]

  def list(): Future[Seq[Factura]] = db.run(facturas.result)

  def listUsuario(user: String): Future[Seq[Factura]] = {
    val action = facturas.filter(f => f.dni === user)
    db.run(action.result)
  }

  def getFactura(fac: Int): Future[Option[Factura]] = {
    val action = facturas.filter(_.id === fac)
    db.run(action.result) map { _.headOption }
  }

  val insertQuery = facturas returning facturas.map(_.id) into ((item, id) => item.copy(id = Some(id)))

  def insert(f: Factura): Future[Factura] = {
    val action = insertQuery += f.copy(id = None)
    db.run(action)
  }
}
