package tables

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import aux.Methods._
import shared.dbclasses.Extra
import scala.util.{Try, Success, Failure}

@Singleton()
class ExtraDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  class Extras(tag: Tag) extends Table[Extra](tag, "EXTRA") {
    def nombre = column[String]("ID", O.PrimaryKey)
    def precio = column[Float]("PRECIO")
    def descripcion = column[String]("DESCRIPCION")

    def * = (nombre,
      precio,
      descripcion) <> ((Extra.apply _).tupled, Extra.unapply _)
  }

  private val extras = TableQuery[Extras]

  def list(): Future[Seq[Extra]] = db.run(extras.result)

  def get(n: String): Future[Float] = {
    val nlower = n.toLowerCase
    val action = extras
      .filter(_.nombre.toLowerCase === nlower)
      .map(_.precio)
    db.run(action.result) map { _.headOption } map { _.getOrElse(0.0f) }
  }
}
