package tables

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import java.sql.Date
import shared.classes.Metodo
import Metodo._
import aux.Methods._
import shared.dbclasses.Pago
import scala.util.{Try, Success, Failure}


@Singleton()
class PagoDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  implicit val metodoColumn = MappedColumnType.base[Metodo, String](applyImpl, applyImpl)

  class Pagos(tag: Tag) extends Table[Pago](tag, "PAGO") {
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def fecha = column[Date]("FECHA")
    def reserva = column[Int]("RESERVA")
    def metodo = column[Metodo]("METODO")
    def coste = column[Float]("COSTE")

    def * = (id.?,
      fecha,
      reserva,
      metodo,
      coste) <> ((Pago.apply _).tupled, Pago.unapply _)
  }

  private val pagos = TableQuery[Pagos]

  def list(): Future[Seq[Pago]] = db.run(pagos.result)

  def listMetalico() = {
    list() map { s => s.filter { p => p.metodo match {
      case Metalico => true
      case _ => false
    }}}
  }
  def listTarjeta() = {
    list() map { s => s.filter { p => p.metodo match {
      case Metalico => false
      case _ => true
    }}}
  }

  val format = new java.text.SimpleDateFormat("yyyy-MM-dd")

  val insertQuery = pagos returning pagos.map(_.id) into ((item, id) => item.copy(id = Some(id)))

  def insert(r: Pago): Future[Pago] = {
    val action = insertQuery += r.copy(id = None)
    db.run(action)
  }

  // Get from range [from, to] (closed interval)
  def rangeDate(from: Date, to: Date): Future[Seq[Pago]] = {
    val action = pagos
      .filter(p => p.fecha >= from && p.fecha <= to)
      .sortBy(_.fecha)
    db.run(action.result)
  }
}
