package tables

import play.api.db.slick.{HasDatabaseConfigProvider, DatabaseConfigProvider}
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import slick.driver.JdbcProfile

trait DBComponent { self: HasDatabaseConfigProvider[JdbcProfile] =>
  import driver.api._
}
