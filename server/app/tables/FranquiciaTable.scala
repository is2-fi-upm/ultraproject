package tables

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import shared.dbclasses.{Franquicia, Coche}
import scala.util.{Try, Success, Failure}

@Singleton()
class FranquiciaDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider, cochesFranquiciaDB: CochesFranquiciaDB, cocheDB: CocheDB) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  class Franquicias(tag: Tag) extends Table[Franquicia](tag, "FRANQUICIA") {
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def calle = column[String]("CALLE")
    def numero = column[Int]("NUMERO")
    def codigopostal = column[Int]("CODIGOPOSTAL")
    def poblacion = column[String]("POBLACION")
    def comunidad = column[String]("COMUNIDAD")
    def pais = column[String]("PAIS")

    def * = (id.?,
      calle,
      numero,
      codigopostal,
      poblacion,
      comunidad,
      pais) <> ((Franquicia.apply _).tupled, Franquicia.unapply _)
  }

  private val franquicias = TableQuery[Franquicias]

  def list(): Future[Seq[Franquicia]] = db.run(franquicias.result)

  def getFranquicia(id: Int): Future[Option[Franquicia]] = {
    val action = franquicias filter { _.id === id}
    db.run(action.result) map { _.headOption}
  }

  def getCoches(id: Int): Future[Seq[Coche]] = {
    cochesFranquiciaDB.getCochesByFranquicia(id) flatMap { cocheDB.getCoches(_) }
  }

  def getPaises(): Future[Seq[String]] = {
    val action = franquicias map { _.pais } distinct

    db.run(action.result)
  }

  def getComunidades(pais: String): Future[Seq[String]] = {
    val action = franquicias filter { _.pais === pais } map { _.comunidad } distinct

    db.run(action.result)
  }

  def getPoblaciones(pais: String, comunidad: String): Future[Seq[String]] = {
    val action = franquicias filter { f => f.pais === pais && f.comunidad === comunidad } map { _.poblacion } distinct

    db.run(action.result)
  }

  def listWithFilter(pais: String, comunidad: String, poblacion: String): Future[Seq[Franquicia]] = {
    val action = franquicias filter { f => f.pais === pais && f.comunidad === comunidad && f.poblacion === poblacion }

    db.run(action.result)
  }

  val insertQuery = franquicias returning franquicias.map(_.id) into ((item, id) => item.copy(id = Some(id)))

  def insert(f: Franquicia): Future[Franquicia] = {
    val action = insertQuery += f.copy(id = None)
    db.run(action)
  }
}
