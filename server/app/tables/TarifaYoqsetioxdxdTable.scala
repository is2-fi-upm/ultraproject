package tables

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import aux.Methods._
import shared.dbclasses.TarifaYoqsetioxdxd
import scala.util.{Try, Success, Failure}

@Singleton()
class TarifaYoqsetioxdxdDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  class TarifasYoqsetioxdxd(tag: Tag) extends Table[TarifaYoqsetioxdxd](tag, "TARIFAYOQSETIOXDXD") {
    def id = column[String]("ID", O.PrimaryKey)
    def factor = column[Float]("FACTOR")

    def * = (id,
      factor) <> ((TarifaYoqsetioxdxd.apply _).tupled, TarifaYoqsetioxdxd.unapply _)
  }

  private val tarifasyoqsetioxdxd = TableQuery[TarifasYoqsetioxdxd]

  def list(): Future[Seq[TarifaYoqsetioxdxd]] = db.run(tarifasyoqsetioxdxd.result)

  def get(n: String): Future[Float] = {
    val nlower = n.toLowerCase
    val action = tarifasyoqsetioxdxd
      .filter(_.id.toLowerCase === nlower)
      .map(_.factor)
    db.run(action.result) map { _.headOption } map { _.getOrElse(1.0f) }
  }
}
