package tables

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import java.sql.Date
import aux.Methods._
import shared.dbclasses.TarifaPeriodo
import scala.util.{Try, Success, Failure}

@Singleton()
class TarifaPeriodoDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  class TarifasPeriodo(tag: Tag) extends Table[TarifaPeriodo](tag, "TARIFAPERIODO") {
    def nombre = column[String]("NOMBRE", O.PrimaryKey)
    def inicio = column[Date]("INICIO")
    def fin = column[Date]("FIN")
    def factor = column[Float]("FACTOR")

    def * = (nombre,
      inicio,
      fin,
      factor) <> ((TarifaPeriodo.apply _).tupled, TarifaPeriodo.unapply _)
  }

  private val tarifasperiodo = TableQuery[TarifasPeriodo]

  def list(): Future[Seq[TarifaPeriodo]] = db.run(tarifasperiodo.result)

  // Returns the factor or 1.0 if wasn't found
  def getFactor(d: Date): Future[Float] = {
    val action = tarifasperiodo
      .filter(tp => tp.inicio <= d && tp.fin >= d)
      .map(_.factor)
    db.run(action.result) map { _.headOption } map { _.getOrElse(1.0f) }
  }
}
