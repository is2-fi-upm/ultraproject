package tables

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import shared.dbclasses.Franquicia
import shared.dbclasses.Coche
import scala.util.{Try, Success, Failure}

@Singleton()
class CochesFranquiciaDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  case class CochesFranquicia(id: Option[Int], franquicia: Int, coche: String)

  class CochesFranquicias(tag: Tag) extends Table[CochesFranquicia](tag, "COCHESFRANQUICIA") {
    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def franquicia = column[Int]("FRANQUICIA")
    def coche = column[String]("COCHE")

    def * = (
      id.?,
      franquicia,
      coche) <> ((CochesFranquicia.apply _).tupled, CochesFranquicia.unapply _)
  }

  private val cochesfranquicias = TableQuery[CochesFranquicias]

  def getCochesByFranquicia(franq: Int): Future[Seq[String]] = {
    val action = cochesfranquicias
      .filter(x => x.franquicia === franq)
      .map(c => c.coche)
    db.run(action.result)
  }

  def getFranquiciaBycoche(coche: String): Future[Option[Int]] = {
    val action = cochesfranquicias
      .filter(x => x.coche === coche)
      .map(c => c.franquicia)
    db.run(action.result) map { _.headOption }
  }

  val insertQuery = cochesfranquicias returning cochesfranquicias.map(_.id)

  def insert(coche: String, franq: Int): Future[Int] = {
    val action = insertQuery += CochesFranquicia(None, franq, coche)
    db.run(action)
  }
}
