package tables

import java.util.Calendar
import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import java.sql.{Timestamp, Date}
import shared.dbclasses.Reserva
import shared.classes.{TipoReserva, Estado}
import TipoReserva._
import Estado._
import aux.Methods._

@Singleton()
class TemporalReservaDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider, reservaDB: ReservaDB, cocheDB: CocheDB) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  case class TemporalReserva(id: String,
    idusuario: String,
    idreservante: String,
    idcoche: String,
    tarjeta: String,
    caducidadtarjeta: Date,
    tipotarjeta: String,
    fechareserva: Timestamp,
    fechainicio: Date,
    tiporeserva: TipoReserva,
    precio: Float,
    created: Timestamp)

  implicit val tiporeservaColumn = MappedColumnType.base[TipoReserva, String](applyImpl, applyImpl)

  class TemporalReservas(tag: Tag) extends Table[TemporalReserva](tag, "TEMPORALRESERVA") {
    def id = column[String]("ID", O.PrimaryKey)
    def idusuario = column[String]("IDUSUARIO")
    def idreservante = column[String]("IDRESERVANTE")
    def idcoche = column[String]("IDCOCHE")
    def tarjeta = column[String]("TARJETA")
    def caducidadtarjeta = column[Date]("CADUCIDADTARJETA")
    def tipotarjeta = column[String]("TIPOTARJETA")
    def fechareserva = column[Timestamp]("FECHARESERVA")
    def fechainicio = column[Date]("FECHAINICIO")
    def tiporeserva = column[TipoReserva]("TIPORESERVA")
    def precio = column[Float]("PRECIO")
    def created = column[Timestamp]("CREATED")

    def * = (id,
      idusuario,
      idreservante,
      idcoche,
      tarjeta,
      caducidadtarjeta,
      tipotarjeta,
      fechareserva,
      fechainicio,
      tiporeserva,
      precio,
      created) <> ((TemporalReserva.apply _).tupled, TemporalReserva.unapply _)
  }

  private val temporalreservas = TableQuery[TemporalReservas]

  def uuid = java.util.UUID.randomUUID.toString

  def calendar = Calendar.getInstance

  def getOwner(id: String): Future[Option[String]] = {
    val action = temporalreservas.filter(_.id === id)
    db.run(action.result) map { _.headOption map { _.idreservante }}
  }

  def insert(res: Reserva): Future[String] = {
    val uui = uuid
    val now = new Timestamp(calendar.getTime().getTime())
    val temp = TemporalReserva(uui,
      res.idusuario,
      res.idreservante,
      res.idcoche,
      res.tarjeta,
      res.caducidadtarjeta,
      res.tipotarjeta,
      res.fechareserva,
      res.fechainicio,
      res.tiporeserva,
      res.precio,
      now)
    val action = temporalreservas += temp
    db.run(action) map { _ => uui }
  }

  def removeOld() = {
    val now = new Timestamp(calendar.getTime().getTime()).getTime()
    val action = temporalreservas
    def filt(l: String) = temporalreservas.filter(_.id === l).delete
    db.run(action.result) flatMap { trsRaw =>
      val trs = trsRaw filter { tr =>
        (now - tr.created.getTime()) >= (5 minutes).toMillis
      }

      trs foreach { p => db.run(filt(p.id)) }
      cocheDB.changeMultiState(trs map {_.idcoche}, Disponible)
    }
  }

  def remove(id: String, cs: Estado = Disponible): Future[Boolean] = {
    val action = temporalreservas.filter(_.id === id)
    val actionremove = temporalreservas.filter(_.id === id).delete
    db.run(action.result) flatMap { _.headOption match {
      case Some(tr) => {
        cocheDB.changeState(tr.idcoche, cs) flatMap { _ =>
          db.run(actionremove) map { r => r > 0}
        }
      }
      case None => Future.successful(false)
    }}
  }

  def checkCreateAndRemove(id: String): Future[Option[Reserva]] = {
    val action = temporalreservas.filter(_.id === id)
    db.run(action.result) flatMap { _.headOption match {
      case Some(res) => {
        val now = new Timestamp(calendar.getTime().getTime())
        val isInTime = (now.getTime() - res.created.getTime()) < (5 minutes).toMillis
        if (isInTime) {
          val reserva = Reserva(None, res.idusuario, res.idreservante, res.idcoche, res.tarjeta, res.caducidadtarjeta, res.tipotarjeta, res.fechareserva, res.fechainicio, res.tiporeserva, res.precio)
          reservaDB.insert(reserva) flatMap { res =>
            remove(id, Reservado) map { _ => Some(res) }
          }
        } else {
          remove(id) map {_ => None}
        }
      }
      case None => Future.successful(None)
    }}
  }
}
