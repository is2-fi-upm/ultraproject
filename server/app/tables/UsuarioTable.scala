package tables

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import shared.dbclasses.Usuario
import shared.classes.TipoUsuario
import TipoUsuario._
import scala.util.{Try, Success, Failure}

@Singleton()
class UsuarioDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  class Usuarios(tag: Tag) extends Table[Usuario](tag, "USUARIO") {
    implicit val tipousuarioColumnType = MappedColumnType.base[TipoUsuario, String](x => x, y => y)


    def dni = column[String]("DNI", O.PrimaryKey)
    def nombre = column[String]("NOMBRE")
    def apellidos = column[String]("APELLIDOS")
    def tipo = column[TipoUsuario]("TIPO")
    def email = column[String]("EMAIL")
    def pass = column[String]("PASS")

    def * = (
      dni,
      nombre,
      apellidos,
      tipo,
      email,
      pass) <> ((Usuario.apply _).tupled, Usuario.unapply _)
  }

  private val usuarios = TableQuery[Usuarios]

  def list(): Future[Seq[Usuario]] = {
    db.run(usuarios.result)
  }

  def getUser(dni: String): Future[Option[Usuario]] = {
    db.run(usuarios.filter(u => u.dni.toLowerCase === dni.toLowerCase).result) map {
      r => r.headOption
    }
  }

  def insert(u: Usuario): Future[Try[Usuario]] = {
    db.run((usuarios += u).asTry).map(_ => Success(u))
  }

  def authenticate(dni: String, password: String): Option[Usuario] = {
    val opu = Await.result(getUser(dni), 500 millis)
    opu filter { _.pass == password }
  }
}
