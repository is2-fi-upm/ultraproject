package tables

import javax.inject.{Inject, Singleton}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import slick.driver.JdbcProfile
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import scala.concurrent._
import scala.concurrent.duration._
import scala.language.postfixOps
import shared.dbclasses.Franquicia
import shared.dbclasses.Coche
import scala.util.{Try, Success, Failure}

@Singleton()
class EmpleadoFranquiciaDB @Inject() (protected val dbConfigProvider: DatabaseConfigProvider) extends DBComponent with HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._

  case class EmpleadoFranquicia(usuario: String, franquicia: Int)

  class EmpleadosFranquicias(tag: Tag) extends Table[EmpleadoFranquicia](tag, "EMPLEADOFRANQUICIA") {
    def usuario = column[String]("USUARIO", O.PrimaryKey)
    def franquicia = column[Int]("FRANQUICIA")

    def * = (
      usuario,
      franquicia) <> ((EmpleadoFranquicia.apply _).tupled, EmpleadoFranquicia.unapply _)
  }

  private val empleadosfranquicias = TableQuery[EmpleadosFranquicias]

  def getFranquicia(user: String): Future[Option[Int]] = {
    val action = empleadosfranquicias
      .filter(x => x.usuario === user)
      .map(_.franquicia)
    db.run(action.result) map { _.headOption }
  }

  def insert(user: String, franq: Int): Future[Int] = {
    val action = empleadosfranquicias.insertOrUpdate(EmpleadoFranquicia(user, franq))
    db.run(action)
  }
}
