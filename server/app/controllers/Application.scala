package controllers

import javax.inject.Inject
import scala.concurrent.Future
import play.api.libs.json._
import play.api.mvc._
import play.api.libs.ws._
import scala.concurrent.ExecutionContext.Implicits.global

// scalastyle:off
class Application @Inject() (ws: WSClient) extends Controller {

  def main(name: String = "") = Action {
    Ok(views.html.hello(name))
  }

  def rxtimer() = Action {
    Ok(views.html.rxtimer())
  }

  def saveCookie(name: String) = Action {
    Ok(views.html.save(name)).withSession(
      "user" -> name
    )
  }

  def loadCookie = Action { request =>
    request.session.get("user").map { user =>
      Ok(views.html.hello(user))
    }.getOrElse {
      Unauthorized("Oops, you don't have any user, use /save/<name>")
    }
  }

  def showCars = Action {
    Ok(views.html.coches())
    // ws.url("/api/v1/testcoches")
    //   .withHeaders("Accept" -> "application/json")
    //   .withRequestTimeout(10000)
    //   .get()
    //   .map { x => Ok(x.body) }
  }
}
