package controllers

import java.text.SimpleDateFormat
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import javax.inject.Inject
import auth.AuthConfigImpl
import play.api.libs.json.Json
import play.api.libs.json.Json._
import scala.concurrent.{Future, Await}
import jp.t2v.lab.play2.auth.AuthElement
import shared.classes.{TipoUsuario, Estado, TipoReserva}
import TipoUsuario._
import Estado._
import TipoReserva._
import tables.{FranquiciaDB, CocheDB, CochesFranquiciaDB, UsuarioDB, TarifaBaseDB, TarifaPeriodoDB, ReservaDB, TemporalReservaDB}
import shared.dbclasses.{Coche, Franquicia, Reserva}
import aux.Methods.userToName
import play.api.data.Form
import play.api.data.Forms._
import java.sql.{Timestamp, Date}
import java.util.Calendar
import scala.language.implicitConversions
import scala.language.postfixOps
import scala.concurrent.duration._

class AwesomeLoggedApplication @Inject() (franquiciaDB: FranquiciaDB, cocheDB: CocheDB, cochesFranquiciaDB: CochesFranquiciaDB, usuarioDB: UsuarioDB, tarifaBaseDB: TarifaBaseDB, tarifaPeriodoDB: TarifaPeriodoDB, reservaDB: ReservaDB, temporalReservaDB: TemporalReservaDB) extends Controller with AuthElement with AuthConfigImpl {
  def validateReserva(id: Option[Int],
    idusuario: String,
    idreservante: String,
    idcoche: String,
    tarjeta: String,
    caducidadtarjeta: Date,
    tipotarjeta: String,
    fechareserva: Timestamp,
    fechainicio: Date,
    tiporeserva: String,
    precio: Float): Option[Reserva] = {
    // Reserva final si todo va bien (menos precio que se calcura e idreservante que se injecta)
    val reserva = Reserva(None, idusuario, idreservante, idcoche, tarjeta, caducidadtarjeta, tipotarjeta, fechareserva, fechainicio, tiporeserva, precio)

    // caducidad posterior o igual a fechainicio
    val testcadtarj = if (caducidadtarjeta.compareTo(fechainicio) >= 0) Some(true) else None
    // fechaInicio posterior a hoy
    val testfecha = if (fechainicio.compareTo(new java.util.Date()) > 0) Some(true) else None

    // Results monadic style
    testcadtarj flatMap {_ => testfecha} flatMap {_ =>
      // coche existe y esta disponible
      val future = cocheDB.getCoche(idcoche) map { cochop => cochop.filter(_.estado.isInstanceOf[Disponible.type])} flatMap {cOpt => cOpt match {
        case Some(c) => {
          // coger precio base
          val baseF = tarifaBaseDB.getPrecio(c.gama, tiporeserva)
          // coger factor de mult
          val factorF = tarifaPeriodoDB.getFactor(fechainicio)
          baseF.zip(factorF) map {
            // aplicar formuliqui
            case (Some(base), factor) => Some(base * factor)
            case _ => None
          }
        }
        case None => Future.successful(None)
      }}
      Await.result(future, Duration(1, SECONDS))
    } map { prec => reserva.copy(precio=prec) }
  }

  implicit def date2sqlDate(d: java.util.Date) = new java.sql.Date(d.getTime())

  def reservaForm = Form {
    mapping(
      "id" -> ignored[Option[Int]](None),
      "idusuario" -> nonEmptyText,
      "idreservante" -> ignored[String](""),
      "idcoche" -> nonEmptyText,
      "tarjeta" -> nonEmptyText,
      "caducidadtarjeta" -> sqlDate("MM/yy"),
      "tipotarjeta" -> nonEmptyText,
      "fechareserva" -> ignored[Timestamp](new Timestamp(new java.util.Date().getTime())),
      "fechainicio" -> sqlDate("dd/MM/yyyy"),
      "tiporeserva" -> nonEmptyText,
      "precio" -> ignored[Float](0.0F)
    )(validateReserva)(_.map(r =>
      (None, r.idusuario, "", r.idcoche, "", r.caducidadtarjeta, "", r.fechareserva, r.fechainicio, r.tiporeserva, 0.0F)))
      .verifying("Invalid reserva", result => result.isDefined)
  }

  def postReserva(id: Int, coche: String) = AsyncStack(AuthorityKey -> Normal) { implicit request =>
    reservaForm.bindFromRequest.fold(
      // Si el form no paso el comprobante
      formWithErrors => Future.successful(BadRequest(views.html.reservar(formWithErrors, coche, "", id, loggedIn.dni, loggedIn.tipo.isInstanceOf[Normal.type], loggedIn.nombre))),
      // Si lo paso vamos por buen camino pero aun hay que comprobar
      reservaOpt => {
        reservaOpt map { _.copy(idreservante=loggedIn.dni) } match {
          case Some(reserva) => {
            // Comprobar que usuario existe
            val testuser = usuarioDB.getUser(reserva.idusuario).zip(usuarioDB.getUser(reserva.idreservante)) map {
              // Si el original es normal el reservado debe ser el mismo
              // Si es una empresa, el reservado debe ser una personal normal
              case (Some(ru), Some(origu)) =>
                ((origu.tipo.isInstanceOf[Normal.type] && ru.dni == origu.dni) ||
                  (origu.tipo.isInstanceOf[Empresa.type] && ru.tipo.isInstanceOf[Normal.type]))
              //  si ya se llegaba mal aqui.. false
              case _ => false
            }

            testuser flatMap {
              // When we are here, everything should be Ok?
              case true => {
                // Marcar coche como reservado
                cocheDB.changeState(reserva.idcoche, Reservado) flatMap { int =>
                  // reservaDB.insert(reserva)
                  temporalReservaDB.insert(reserva) map { temporalID =>
                    Redirect(s"/aceptarReserva/$temporalID?idusuario=${reserva.idusuario}&idreservante=${reserva.idreservante}&idcoche=${reserva.idcoche}&fechainicio=${reserva.fechainicio}&tiporeserva=${reserva.tiporeserva}&precio=${reserva.precio}")
                  }
                }
              }
              case false => Future.successful(BadRequest("Something went really really bad"))
            }
          }
          case None => Future.successful(BadRequest("This should never happend"))
        }
      }
    )
  }

  def postAceptarReserva(tid: String) = AsyncStack(AuthorityKey -> Normal) { implicit request =>
    temporalReservaDB.checkCreateAndRemove(tid) map {
      case Some(reserva) => {
        cocheDB.changeReserva(reserva.idcoche, reserva.id.get)
        Ok(toJson(s"/reserva/${reserva.id.get}"))
      }
      case None => Ok(toJson("/aceptarReserva/timeexpired"))
    }
  }

  def postCancelarReserva(tid: String) = AsyncStack(AuthorityKey -> Normal) { implicit request =>
    temporalReservaDB.remove(tid) map (_ => Ok(toJson("/")))
  }

  def aceptarReservaTimeExpired() = Action {
    Ok("yoqsetio")
  }

  def aceptarReserva(tid: String,
    idusuario: String,
    idreservante: String,
    idcoche: String,
    fechainicio: Date,
    tiporeserva: TipoReserva,
    precio: Float) = AsyncStack(AuthorityKey -> Normal) { implicit request =>
    temporalReservaDB.removeOld() flatMap {_ =>
      temporalReservaDB.getOwner(tid) map {
        case Some(l) => {
          if (loggedIn.dni == l) {
            val dateformat = new SimpleDateFormat("dd/MM/yyy")
            Ok(views.html.acceptReserva(tid, idusuario, idreservante, idcoche, dateformat.format(fechainicio), tiporeserva, precio, loggedIn.nombre))
          } else {
            BadRequest(views.html.badrequest())
          }
        }
        case None => Redirect("/aceptarReserva/timeexpired")
      }
    }
  }

  def reservarCocheFranquicia(franq: Int, coche: String) = AsyncStack(AuthorityKey -> Normal) { implicit request =>
    case class Container(coche: Coche, franquicia: Franquicia)

    val cocheFranquicia  = cocheDB.getCoche(coche) flatMap { cocheOpt =>
      franquiciaDB.getFranquicia(franq) map { franqOpt => (cocheOpt, franqOpt) match {
        case (None, _) => None
        case (_, None) => None
        case (Some(coche), Some(franquicia)) => Some(Container(coche, franquicia))
      }}
    }

    val franqByCar = cochesFranquiciaDB.getFranquiciaBycoche(coche)

    cocheFranquicia flatMap { cf =>
      franqByCar map { realRelation => (cf, realRelation) match {
        case (None, _) => NotFound(views.html.notFound(request.path))
        case (_, None) => NotFound(views.html.notFound(request.path))
        case (Some(Container(_, franquicia)), Some(fid)) if fid != franquicia.id.getOrElse(-1) => NotFound(views.html.notFound(request.path))
        case (Some(Container(coche, _)), Some(_)) if !coche.estado.isInstanceOf[Disponible.type] => NotFound(views.html.notFound(request.path))
        case (Some(Container(coche, franquicia)), Some(_)) => {
          // Here is ok everything!
          Ok(views.html.reservar(reservaForm, coche.matricula, coche.gama, franquicia.id.get, loggedIn.dni, loggedIn.tipo.isInstanceOf[Normal.type], loggedIn.nombre))
        }
      }}
    }
  }


  def profile() = AsyncStack(AuthorityKey -> Normal) { implicit request =>
    usuarioDB.getUser(loggedIn.dni) map {
      case Some(user) => {
        Ok(views.html.profile(user.nombre, user.apellidos, user.dni, user.tipo, user.email))
      }
      case None => Redirect("/login")
    }
  }

  def reserva(id: Int) = AsyncStack(AuthorityKey -> Normal) { implicit request =>
    reservaDB.getReserva(id) map {
      case Some(reserva) => (loggedIn.dni == reserva.idusuario) || (loggedIn.dni == reserva.idreservante) match {
        case true => {
          val cal = Calendar.getInstance()
          cal.setTime(reserva.fechainicio)
          Ok(views.html.showreserva(
            reserva.id.get,
            reserva.idusuario,
            reserva.idreservante,
            reserva.idcoche,
            "****" + reserva.tarjeta.substring(math.max(reserva.tarjeta.length - 4, 0), reserva.tarjeta.length),
            // s"${reserva.fechainicio.getDate()}/${reserva.fechainicio.getMonth()}/${reserva.fechainicio.getYear()}",
            s"${cal.get(Calendar.DAY_OF_MONTH)}/${cal.get(Calendar.MONTH)}/${cal.get(Calendar.YEAR)}",
            reserva.tiporeserva,
            reserva.precio,
            loggedIn.nombre))
        }
        case falte => Redirect("/profile")
      }
      case None => Redirect("/profile")
    }
  }
}
