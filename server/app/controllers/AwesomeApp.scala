package controllers

import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import javax.inject.Inject
import tables.{FranquiciaDB, TemporalReservaDB}
import auth.AuthConfigImpl
import jp.t2v.lab.play2.auth.OptionalAuthElement
import aux.Methods.{userToName, userToType}

class AwesomeApplication @Inject() (franquiciaDB: FranquiciaDB, temporalReservaDB: TemporalReservaDB) extends Controller with OptionalAuthElement with AuthConfigImpl {

  def main() = StackAction { implicit request =>
    Ok(views.html.newmain(userToName(loggedIn), userToType(loggedIn)))
  }

  def showCochesFranquicia(id: Int) = AsyncStack { implicit request =>
    temporalReservaDB.removeOld() flatMap { _ =>
      franquiciaDB.getFranquicia(id) map {
        case Some(f) => Ok(views.html.showfranquicia(id, f.calle, f.numero, f.pais, f.comunidad, f.poblacion, f.codigopostal, userToName(loggedIn), userToType(loggedIn)))
        case None => NotFound(views.html.notFound(request.path))
      }
    }
  }
}
