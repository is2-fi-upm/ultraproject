package controllers

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import play.api.libs.json.Json
import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfig
import play.api.mvc.Results._
import play.api.mvc.Action
import play.api.mvc.Controller
import javax.inject.Inject
import slick.driver.JdbcProfile
// import shared.dbclasses.Coche
// import tables.CocheDB
// import shared.classes.Estado
import tables.UsuarioDB
import aux.Implicits._
import aux.Methods.userToName
import scala.util.Success
import scala.util.Failure
import scala.util.{Try, Success, Failure}
import auth.AuthConfigImpl
import jp.t2v.lab.play2.auth._
import play.api.data.Form
import play.api.data.Forms._

import jp.t2v.lab.play2.auth.OptionalAuthElement
import shared.dbclasses.Usuario
import shared.classes.TipoUsuario
import TipoUsuario._

class LoginLogoutApplication @Inject() (usuarioDB: UsuarioDB) extends Controller with OptionalAuthElement with LoginLogout with AuthConfigImpl {

  /** Your application's login form.  Alter it to fit your application */
  val loginForm = Form {
    mapping("dni" -> nonEmptyText, "password" -> nonEmptyText)(usuarioDB.authenticate)(_.map(u => (u.dni, "")))
      .verifying("Invalid dni or password", result => result.isDefined)
  }

  def login = StackAction { implicit request =>
    if (loggedIn.isDefined)
      Redirect("/")
    else
      Ok(views.html.login(loginForm, userToName(loggedIn)))
    // val errormsg = loginForm.globalError map { _.message } getOrElse ""
  }

  def signUpAuthenticate(nombre: String,
    apellidos: String,
    dni: String,
    email: String,
    password: String,
    passwordConfirm: String,
    tipoCliente: String): Option[Usuario] = {
    val result = Usuario(dni, nombre, apellidos, tipoCliente, email, password)
    val passEq = if (password == passwordConfirm) Some(true) else None

    val passLength = if(password.length() >= 7) Some(true) else None

    passEq flatMap { _ => passLength } map { _ => result }
  }

  def signupForm = Form {
    mapping(
      "nombre" -> nonEmptyText,
      "apellidos" -> nonEmptyText,
      "dni" -> nonEmptyText,
      "email" -> email,
      "password" -> nonEmptyText,
      "passwordConfirm" -> nonEmptyText,
      "tipoCliente" -> nonEmptyText
    )(signUpAuthenticate)(_.map(u => (u.nombre, u.apellidos, u.dni, u.email, "", "", u.tipo))).verifying("Invalid User", result => result.isDefined)
  }

  /**
    * Return the `gotoLogoutSucceeded` method's result in the logout action.
    *
    * Since the `gotoLogoutSucceeded` returns `Future[Result]`,
    * you can add a procedure like the following.
    *
    *   gotoLogoutSucceeded.map(_.flashing(
    *     "success" -> "You've been logged out"
    *   ))
    */
  def logout = Action.async { implicit request =>
    // do something...
    gotoLogoutSucceeded
  }

  /**
    * Return the `gotoLoginSucceeded` method's result in the login action.
    *
    * Since the `gotoLoginSucceeded` returns `Future[Result]`,
    * you can add a procedure like the `gotoLogoutSucceeded`.
    */
  def authenticate = Action.async { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => Future.successful(BadRequest(views.html.login(formWithErrors, ""))),
      user => gotoLoginSucceeded(user.get.dni)
    )
  }

  def signup = Action.async { implicit request =>
    signupForm.bindFromRequest.fold(
      formWithErrors => Future.successful(BadRequest(views.html.login(formWithErrors, ""))),
      user => {
        usuarioDB.getUser(user.get.dni) flatMap { userOpt => userOpt match {
          case Some(_) => Future.successful(Redirect("/login"))
          case None => {
            usuarioDB.insert(user.get) flatMap { res =>
              if (res.isSuccess)
                gotoLoginSucceeded(user.get.dni)
              else
                Future.successful(Redirect("/login"))
            }
          }
        }}
      }
    )

  }
}
