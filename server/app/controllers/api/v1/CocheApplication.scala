package controllers.api.v1

import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import play.api.libs.json.Json
import play.api.Play
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfig
import play.api.mvc.Results._
import play.api.mvc.Action
import play.api.mvc.Controller
import javax.inject.Inject
import slick.driver.JdbcProfile
import shared.dbclasses.Coche
import tables.{CocheDB, UsuarioDB}
import shared.classes.Estado
import shared.classes.TipoUsuario
import TipoUsuario._
import aux.Implicits._
import scala.util.Success
import scala.util.Failure
import scala.util.{Try, Success, Failure}

import jp.t2v.lab.play2.auth.AuthElement
import auth.AuthConfigImpl

class CocheApplication @Inject() (cocheDB: CocheDB, usuarioDB: UsuarioDB) extends Controller with AuthElement with AuthConfigImpl {

  def get = Action.async { _ =>
    cocheDB.list().map(c => Ok(toJson(c)))
  }

  def getMatriculas(onlyDisp: Boolean = false, onlyRes: Boolean = false, onlyCarr: Boolean = false) = Action.async { _ =>
    cocheDB.listIDs(onlyDisp, onlyRes, onlyCarr).map(c => Ok(toJson(c)))
  }

  def getCoche(matr: String) = Action.async { _ =>
    cocheDB.getCoche(matr) map {
      case Some(coche) => Ok(toJson(coche))
      case None => BadRequest(Json.obj("status" -> "Error", "message" -> "Coche no existe"))
    }
  }

  // The `StackAction` method
  //    takes `(AuthorityKey, Authority)` as the first argument and
  //    a function signature `RequestWithAttributes[AnyContent] => Result` as the second argument and
  //    returns an `Action`

  // The `loggedIn` method
  //     returns current logged in user

  def post = AsyncStack(AuthorityKey -> Empleado) { implicit request =>
    request.body.asJson map { json =>
      json.validate[Coche] map { coche =>
        cocheDB.insert(coche) map { result => result match {
          case Success(newcoche) => Ok(toJson(newcoche))
          case Failure(e) => BadRequest(Json.obj("status" -> "Error", "message" -> "Some error"))
        }}
      } getOrElse Future.successful(BadRequest(Json.obj("status" -> "Error", "message" -> "I can't validate the JSON")))
    } getOrElse Future.successful(BadRequest(Json.obj("status" -> "Error", "message" -> "The body was not json (content-type maybe?)")))
  }

  case class ChangeStateJson(estado: Estado)
  implicit val changeStateF = Json.format[ChangeStateJson]

  def changeState(matricula: String) = AsyncStack(AuthorityKey -> Empleado) { implicit request =>
    request.body.asJson map { json =>
      json.validate[ChangeStateJson] map { cambio =>
        cocheDB.changeState(matricula, cambio.estado) map { c =>
          if (c > 0)
            Ok(Json.obj("status" -> "Ok", "message" -> "Updated"))
          else Ok(Json.obj("status" -> "Error", "message" -> "Can't update"))
        }
      } getOrElse Future.successful(BadRequest(Json.obj("status" -> "Error", "message" -> "I can't validate the JSON")))
    } getOrElse Future.successful(BadRequest(Json.obj("status" -> "Error", "message" -> "The body was not json (content-type maybe?)")))
  }

  // def changeState(matricula: String) = Action.async(parse.json) { implicit request =>
  //   // TODO Authentication
  //   request.body.validate[ChangeStateJson] map { cambio =>
  //     cocheDB.changeState(matricula, cambio.estado) map { c =>
  //       if (c > 0)
  //         Ok(Json.obj("status" -> "Ok", "message" -> "Updated"))
  //       else Ok(Json.obj("status" -> "Error", "message" -> "Can't update"))
  //     }
  //   } getOrElse Future.successful(BadRequest(Json.obj("status" -> "Error", "message" -> "Invalid JSON")))
  // }
}
