package controllers.api.v1

import javax.inject.Inject
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import play.api.libs.json.Json
import play.api.Play
import play.api.mvc.Results._
import play.api.mvc.Action
import play.api.mvc.Controller
import tables.FacturaDB
import aux.Implicits._

class FacturaApplication @Inject() (facturaDB: FacturaDB) extends Controller {
  def get = Action.async { _ =>
    facturaDB.list() map { f => Ok(toJson(f)) }
  }

  def getFacturasUsuario(user: String) = Action.async { _ =>
    facturaDB.listUsuario(user) map { f => Ok(toJson(f)) }
  }

  def getFactura(id: Int) = Action.async { _ =>
    facturaDB.getFactura(id) map {
      case Some(fac) => Ok(toJson(fac))
      case None => BadRequest(Json.obj("status" -> "Error", "message" -> "Factura no existe"))
    }
  }
}
