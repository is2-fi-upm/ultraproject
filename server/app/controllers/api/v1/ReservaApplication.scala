package controllers.api.v1

import auth.AuthConfigImpl
import javax.inject.Inject
import jp.t2v.lab.play2.auth.AuthElement
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import play.api.libs.json.Json
import play.api.Play
import play.api.mvc.Results._
import play.api.mvc.Action
import play.api.mvc.Controller
import shared.classes.TipoUsuario
import TipoUsuario._
import tables.{ReservaDB, CocheDB}
import aux.Implicits._

class ReservaApplication @Inject() (reservaDB: ReservaDB, cocheDB: CocheDB) extends Controller with AuthElement with AuthConfigImpl {
  def get = Action.async { _ =>
    reservaDB.list() map { f => Ok(toJson(f)) }
  }

  def getReservasUsuario = AsyncStack(AuthorityKey -> Normal) { implicit request =>
    reservaDB.listUsuario(loggedIn.dni) map { f => Ok(toJson(f map { res =>
      Json.obj(
        "id" -> res.id.get,
        "coche" -> res.idcoche,
        "inicio" -> res.fechainicio,
        "tipo" -> res.tiporeserva,
        "precio" -> res.precio
      )
    }
    )) }
  }

  def getReservaCoche(matricula: String) = AsyncStack(AuthorityKey -> Empleado) { implicit request =>
    cocheDB.getCoche(matricula) flatMap {
      case Some(coche) => {
        reservaDB.getReserva(coche.reserva) map {
          case Some(reserva) => Ok(Json.obj(
            "reserva" -> reserva.id.get,
            "precio" -> reserva.precio
          ))
          case None => BadRequest(Json.obj("status" -> "Error", "message" -> "Coche no reservado"))
        }
      }
      case None => Future.successful(BadRequest(Json.obj("status" -> "Error", "message" -> "Coche no existe")))

    }
  }

  def getReserva(id: Int) = AsyncStack(AuthorityKey -> Normal) { implicit request =>
    reservaDB.getReserva(id) map {
      case Some(reserva) => { (loggedIn.dni == reserva.idusuario) || (loggedIn.dni == reserva.idreservante) match {
        case true => Ok(toJson(reserva))
        case falte => BadRequest(Json.obj("status" -> "Error", "message" -> "No puedes verlo"))
      }

      }
      case None => BadRequest(Json.obj("status" -> "Error", "message" -> "Reserva no existe"))
    }
  }

  // TODO reservar
}
