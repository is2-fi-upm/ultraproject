package controllers.api.v1

import javax.inject.Inject
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import play.api.libs.json.Json
import play.api.Play
import play.api.mvc.Results._
import play.api.mvc.Action
import play.api.mvc.Controller
import java.sql.Date
import aux.Implicits._
import tables.TarifaYoqsetioxdxdDB

class TarifaYoqsetioxdxdApplication @Inject() (tarifaYoqsetioxdxdDB: TarifaYoqsetioxdxdDB) extends Controller {
  def get = Action.async { _ =>
    tarifaYoqsetioxdxdDB.list() map (c => Ok(toJson(c)))
  }

  def getFactor(name: String) = Action.async { _ =>
    tarifaYoqsetioxdxdDB.get(name) map { c => Ok(toJson(c)) }
    // tarifaYoqsetioxdxdDB.get(name) map { c => Ok(toJson(c - (c % 0.01))) }
  }
}
