package controllers.api.v1

import javax.inject.Inject
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import play.api.libs.json.Json
import play.api.Play
import play.api.mvc.Results._
import play.api.mvc.Action
import play.api.mvc.Controller
import java.sql.Date
import tables.PagoDB
import aux.Implicits._

class PagosApplication @Inject() (pagoDB: PagoDB) extends Controller {
  def get = Action.async { _ =>
    pagoDB.list() map {f => Ok(toJson(f)) }
  }

  def getMetalico = Action.async { _ =>
    pagoDB.listMetalico() map { f => Ok(toJson(f)) }
  }

  def getTarjeta = Action.async { _ =>
    pagoDB.listTarjeta() map { f => Ok(toJson(f)) }
  }

  def datesFrom(from: Date, to: Date) = Action.async { _ =>
    pagoDB.rangeDate(from, to) map { f => Ok(toJson(f)) }
  }
}
