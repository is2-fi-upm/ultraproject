package controllers.api.v1

import javax.inject.Inject
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import play.api.libs.json.Json
import play.api.Play
import play.api.mvc.Results._
import play.api.mvc.Action
import play.api.mvc.Controller
import java.sql.Date
import aux.Implicits._
import tables.ExtraDB

class ExtraApplication @Inject() (extraDB: ExtraDB) extends Controller {
  def get = Action.async { _ =>
    extraDB.list() map (c => Ok(toJson(c)))
  }

  def getPrecio(name: String) = Action.async { _ =>
    extraDB.get(name) map { c => Ok(toJson(c)) }
  }
}
