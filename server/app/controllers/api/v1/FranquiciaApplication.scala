package controllers.api.v1

import javax.inject.Inject
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import play.api.libs.json._
import play.api.Play
import play.api.mvc.Results._
import play.api.mvc.Action
import play.api.mvc.Controller
import shared.dbclasses.Franquicia
import tables.FranquiciaDB
import aux.Implicits._

class FranquiciaApplication @Inject() (franquiciaDB: FranquiciaDB) extends Controller {
  def toOkJson[T](k: T)(implicit f: Format[T]) = Ok(toJson(k))

  def get = Action.async { _ =>
    franquiciaDB.list() map { f => Ok(toJson(f)) }
  }

  def getCoches(franq: Int) = Action.async { _ =>
    franquiciaDB.getCoches(franq) map { f => Ok(toJson(f)) }
  }

  def getBy(pais: String = "", comunidad: String = "", poblacion: String = "") = Action.async { _ =>
    (pais, comunidad, poblacion) match {
      case ("", _, _) => franquiciaDB.getPaises() map toOkJson[Seq[String]]
      case (x, "", _) => franquiciaDB.getComunidades(x) map toOkJson[Seq[String]]
      case (x, y, "") => franquiciaDB.getPoblaciones(x, y) map toOkJson[Seq[String]]
      case (x, y, z) => franquiciaDB.listWithFilter(x, y, z) map toOkJson[Seq[Franquicia]]
    }
  }
}
