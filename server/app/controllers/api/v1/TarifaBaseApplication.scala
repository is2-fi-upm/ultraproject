package controllers.api.v1

import javax.inject.Inject
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import play.api.libs.json.Json
import play.api.Play
import play.api.mvc.Results._
import play.api.mvc.Action
import play.api.mvc.Controller
import java.sql.Date
import shared.classes.{Gama, TipoReserva}
import Gama._
import TipoReserva._
import aux.Implicits._
import tables.TarifaBaseDB

class TarifaBaseApplication @Inject() (tarifaBaseDB: TarifaBaseDB) extends Controller {
  def get = Action.async { _ =>
    tarifaBaseDB.list() map (c => Ok(toJson(c)))
  }

  def getPrecio(g: Gama, t: TipoReserva) = Action.async { _ =>
    tarifaBaseDB.getPrecio(g, t) map { c => Ok(toJson(c)) }
  }
}
