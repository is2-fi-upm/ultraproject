package controllers.api.v1

import javax.inject.Inject
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import play.api.libs.json.Json
import play.api.Play
import play.api.mvc.Results._
import play.api.mvc.Action
import play.api.mvc.Controller
import java.sql.Date
import shared.classes.{Gama, TipoReserva}
import Gama._
import TipoReserva._
import aux.Implicits._
import tables.{TarifaBaseDB, TarifaPeriodoDB, CocheDB}

class PrecioApplication @Inject() (tarifaBaseDB: TarifaBaseDB, tarifaPeriodo: TarifaPeriodoDB, cocheDB: CocheDB) extends Controller {
  def get(coche: String, fechaInit: Date, tipoRes: TipoReserva) = Action.async { _ =>
    cocheDB.getCoche(coche) flatMap { cocheOpt => cocheOpt match {
      case Some(c) => {
        tarifaBaseDB.getPrecio(c.gama, tipoRes) flatMap { tarifOpt =>
          tarifaPeriodo.getFactor(fechaInit) map { factor =>
            val original = tarifOpt.getOrElse(0.0f) * factor
            val truncated = BigDecimal.decimal(original).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
            Ok(toJson(truncated))
          }
        }
      }
      case None => Future.successful(Ok(toJson(0.0f)))
    }}
  }
}
