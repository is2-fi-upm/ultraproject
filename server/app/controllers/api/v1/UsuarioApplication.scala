package controllers.api.v1

import javax.inject.Inject
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import play.api.Play
import play.api.mvc.Results._
import play.api.mvc.Action
import play.api.mvc.Controller
import tables.UsuarioDB
import aux.Implicits._

class UsuarioApplication @Inject() (usuarioDB: UsuarioDB) extends Controller {
  def get = Action.async { _ =>
    usuarioDB.list().map(users => Ok(toJson(users)))
  }

  def getUser(dni: String) = Action.async { _ =>
    usuarioDB.getUser(dni) map { user => Ok(toJson(user)) }
  }
}
