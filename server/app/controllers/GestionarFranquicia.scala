package controllers

import java.text.SimpleDateFormat
import play.api.mvc._
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import javax.inject.Inject
import auth.AuthConfigImpl
import play.api.libs.json.Json
import play.api.libs.json.Json._
import scala.concurrent.{Future, Await}
import jp.t2v.lab.play2.auth.AuthElement
import shared.classes.{TipoUsuario, Estado, TipoReserva, Metodo}
import TipoUsuario._
import Estado._
import TipoReserva._
import Metodo._
import tables.{FranquiciaDB, CocheDB, CochesFranquiciaDB, UsuarioDB, TarifaBaseDB, TarifaPeriodoDB, ReservaDB, TemporalReservaDB, EmpleadoFranquiciaDB, PagoDB, FacturaDB}
import shared.dbclasses.{Coche, Franquicia, Reserva, Pago, Factura}
import aux.Methods.userToName
import play.api.data.Form
import play.api.data.Forms._
import java.sql.{Timestamp, Date}
import scala.language.implicitConversions
import scala.language.postfixOps
import scala.concurrent.duration._
import scala.util.{Success, Failure}

class GestionarFranquicia @Inject() (empleadoFranquiciaDB: EmpleadoFranquiciaDB, cocheDB: CocheDB, cocheFranquiciaDB: CochesFranquiciaDB, pagoDB: PagoDB, facturaDB: FacturaDB) extends Controller with AuthElement with AuthConfigImpl {

  def validateCreateCar(matricula: String, marca: String, modelo: String, gama: String, foto: String): Option[Coche] = {
    // Check matricula not used?
    Some(Coche(
      matricula,
      marca,
      modelo,
      gama,
      foto,
      Disponible,
      -1
    ))
  }

  def createCarForm = Form {
    mapping(
      "matricula" -> nonEmptyText,
      "marca" -> nonEmptyText,
      "modelo" -> nonEmptyText,
      "gama" -> nonEmptyText,
      "foto" -> ignored[String]("")
    )(validateCreateCar)(_.map(r => (r.matricula, r.marca, r.modelo, r.gama, ""))).verifying("Invalid car", result => result.isDefined)
  }

  def postCreateCar = AsyncStack(AuthorityKey -> Empleado) {
    implicit request =>
    request.body.asMultipartFormData match {
      case Some(body) => {
        createCarForm.bindFromRequest.fold(
          formWithErrors => Future.successful(BadRequest("Error")),
          reservaOpt => (reservaOpt, body.file("foto")) match {
            case (Some(coche), Some(foto)) => {
              val fname = s"${coche.matricula}-${coche.gama}_${coche.modelo}.jpg"

              val imageFile = new java.io.File(s"public/cars/$fname")
              foto.ref.moveTo(imageFile)
              val newcoche = coche.copy(foto = fname)
              empleadoFranquiciaDB.getFranquicia(loggedIn.dni) flatMap {
                case Some(fid) => (cocheDB.insert(newcoche) zip cocheFranquiciaDB.insert(newcoche.matricula, fid)) map {
                  case (Success(car), n) if n > 0 => Ok(newcoche.matricula)
                  case (Success(car), n) if n <= 0 => BadRequest("Remove car")
                  case (Failure(_), _) => BadRequest("Some error")
                }
                case None => Future.successful(BadRequest("Not logged"))
              }
            }
            case _ => Future.successful(BadRequest("Something went wrong"))
          }
        )
      }
      case None => Future.successful(BadRequest(""))
    }
  }

  case class PagoFormData(
    matricula: String,
    reserva: Int,
    coste: Float,
    metodo: Metodo,
    factura: Boolean,
    nombre: String,
    dni: String
  )

  def validatePayment(matricula: String, reserva: String, coste: String, metodo: String, factura: Boolean, nombre: Option[String], dni: Option[String]): Option[PagoFormData] = {
    Some(PagoFormData(matricula, reserva.trim().toInt, coste.trim().toFloat, metodo, factura, nombre.getOrElse(""), dni.getOrElse("")))
  }

  def createPaymentform = Form {
    mapping(
      "matricula" -> nonEmptyText,
      "reserva" -> nonEmptyText,
      "coste" -> nonEmptyText,
      "metodo" -> nonEmptyText,
      "factura" -> boolean,
      "nombre" -> optional(text),
      "dni" -> optional(text)
    )(validatePayment)(_.map(r => ("", "", "", r.metodo, false, None, None)))
    .verifying("Invalid payment", result => result.isDefined)
  }

  def postCreatePayment = AsyncStack(AuthorityKey -> Empleado) {
    implicit request =>

    request.body.asMultipartFormData match {
      case Some(body) => {
        createPaymentform.bindFromRequest.fold(
          formWithErrors => Future.successful(BadRequest("Error")),
          paymentOpt => {
            val pay = paymentOpt.get
            empleadoFranquiciaDB.getFranquicia(loggedIn.dni) flatMap {
              case Some(fid) => {
                val pago = Pago(None, new java.sql.Date(new java.util.Date().getTime()), pay.reserva, pay.metodo, pay.coste)
                val fact = Factura(None, pay.nombre, pay.dni, new java.sql.Timestamp(new java.util.Date().getTime()), pay.matricula, pay.coste, pay.metodo, fid)

                pagoDB.insert(pago) map { p =>
                  var factID = -1
                  if (pay.factura) {
                    val f = Await.result(facturaDB.insert(fact), 1 seconds)
                    factID = f.id.get
                  }

                  Await.result(cocheDB.payCarFranq(pay.matricula), 1 seconds)
                  Await.result(cocheFranquiciaDB.insert(pay.matricula, fid), 1 seconds)

                  Ok(Json.obj("coche" -> pay.matricula, "factura" -> factID))
                }
              }
              case None => Future.successful(BadRequest("Not logged"))
            }
          }
        )
      }
      case None => Future.successful(BadRequest("Not JSON"))
    }
  }

  def getGestion = AsyncStack(AuthorityKey -> Empleado) { implicit request =>
    empleadoFranquiciaDB.getFranquicia(loggedIn.dni) map {
      case Some(fid) => Ok(views.html.gestion(fid, loggedIn.nombre, loggedIn.tipo))
      case None => BadRequest("Really bad >.>")
    }
  }

}
