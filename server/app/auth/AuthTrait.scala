package auth

// Example
import jp.t2v.lab.play2.auth._
import javax.inject.Inject
import scala.concurrent.Future
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json.toJson
import scala.concurrent.{Future, ExecutionContext}
import scala.reflect._
import play.api.mvc.RequestHeader
import play.api.mvc.Results._
import play.api.mvc._
import play.api._

import tables.UsuarioDB
import shared.dbclasses.Usuario
import shared.classes.TipoUsuario
import TipoUsuario._
import aux.Implicits._

trait AuthConfigImpl extends AuthConfig {
  @Inject var usuarioDB: UsuarioDB = null
  /**
    * A type that is used to identify a user.
    * `String`, `Int`, `Long` and so on.
    */
  type Id = String

  /**
    * A type that represents a user in your application.
    * `User`, `Account` and so on.
    */
  type User = Usuario

  /**
    * A type that is defined by every action for authorization.
    * This sample uses the following trait:
    *
    * sealed trait Role
    * case object Administrator extends Role
    * case object NormalUser extends Role
    */
  type Authority = TipoUsuario

  /**
    * A `ClassTag` is used to retrieve an id from the Cache API.
    * Use something like this:
    */
  val idTag: ClassTag[Id] = classTag[Id]

  /**
    * The session timeout in seconds
    */
  val sessionTimeoutInSeconds: Int = 3600

  /**
    * A function that returns a `User` object from an `Id`.
    * You can alter the procedure to suit your application.
    */
  def resolveUser(id: Id)(implicit ctx: ExecutionContext): Future[Option[User]] =
    usuarioDB.getUser(id)

  /**
    * Where to redirect the user after a successful login.
    */
  def loginSucceeded(request: RequestHeader)(implicit ctx: ExecutionContext): Future[Result] = {
    val refererUri = request.headers.get("Referer").getOrElse("/")
    val uri = request.session.get("access_uri").getOrElse(refererUri)
    Future.successful(Redirect(uri).withSession(request.session - "access_uri"))
  }
    // Future.successful(Redirect(routes.Message.main))

  /**
    * Where to redirect the user after logging out
    */
  def logoutSucceeded(request: RequestHeader)(implicit ctx: ExecutionContext): Future[Result] =
    Future.successful(Redirect("/"))
    // Future.successful(Redirect(routes.Application.login))

  /**
    * If the user is not logged in and tries to access a protected resource then redirect them as follows:
    */
  def authenticationFailed(request: RequestHeader)(implicit ctx: ExecutionContext): Future[Result] =
    Future.successful(Redirect("/login#login").withSession("access_uri" -> request.uri))
    // Future.successful(Redirect(routes.Application.login))

  /**
    * If authorization failed (usually incorrect password) redirect the user as follows:
    */
  override def authorizationFailed(request: RequestHeader, user: User, authority: Option[Authority])(implicit context: ExecutionContext): Future[Result] = {
    Future.successful(Forbidden("You don't have any power here!"))
  }

  /**
    * A function that determines what `Authority` a user has.
    * You should alter this procedure to suit your application.
    */
  def authorize(user: User, authority: Authority)(implicit ctx: ExecutionContext): Future[Boolean] = Future.successful {
    (user.tipo, authority) match {
      case (Admin, _) => true
      case (Empleado, Empleado) => true
      case (Empleado, Empresa) => true
      case (Empleado, Normal) => true
      case (Empresa, Empresa) => true
      case (Empresa, Normal) => true
      case (Normal, Normal) => true
      case _ => false
    }
  }

  /**
    * (Optional)
    * You can custom SessionID Token handler.
    * Default implementation use Cookie.
    */
  override lazy val tokenAccessor = new CookieTokenAccessor(
    /*
     * Whether use the secure option or not use it in the cookie.
     * Following code is default.
     */
    cookieSecureOption = play.api.Play.isProd(play.api.Play.current),
    cookieMaxAge       = Some(sessionTimeoutInSeconds)
  )
}
