package aux

object Methods {
  def applyImpl[T, R] (implicit f: T => R): (T => R) = (x: T) => f(x)
  def userToName(u: Option[shared.dbclasses.Usuario]) = u map { _.nombre } getOrElse ""
  def userToType(u: Option[shared.dbclasses.Usuario]) = u map { _.tipo } getOrElse shared.classes.TipoUsuario.Anonimo
}
