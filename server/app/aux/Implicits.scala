package aux

import play.api.libs.json._
import java.util.Base64
import java.sql.Timestamp
import java.sql.Blob
import javax.sql.rowset.serial.SerialBlob
import java.text.SimpleDateFormat
import shared.dbclasses._
import shared.classes._
import Gama._
import Estado._
import TipoUsuario._
import TipoReserva._
import Metodo._

object Implicits {
  // Timestamp
  implicit object timestampFormat extends Format[Timestamp] {
    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'")
    def reads(json: JsValue) = {
      val str = json.as[String]
      JsSuccess(new Timestamp(format.parse(str).getTime))
    }
    def writes(ts: Timestamp) = JsString(format.format(ts))
  }

  // Blob
  implicit object blobFormat extends Format[Blob] {
    def reads(json: JsValue) = {
      val str = json.as[String]
      JsSuccess(new SerialBlob(Base64.getDecoder.decode(str)))
    }

    def writes(b: Blob) = {
      JsString(Base64.getEncoder.encodeToString(b.getBytes(1, b.length.toInt)))
    }
  }

  // Gama
  implicit object gamaFormat extends Format[Gama] {
    def reads(json: JsValue) = json match {
      case JsString(x) => JsSuccess(x)
      case _ => JsError("cannot parse it")
    }
    def writes(g: Gama) = JsString(g)
  }

  // Estado
  implicit object estadoFormat extends Format[Estado] {
    def reads(json: JsValue) = json match {
      case JsString(x) => JsSuccess(x)
      case _ => JsError("cannot parse it")
    }
    def writes(g: Estado) = JsString(g)
  }

  // TipoUsuario
  implicit object tipousuarioFormat extends Format[TipoUsuario] {
    def reads(json: JsValue) = json match {
      case JsString(x) => JsSuccess(x)
      case _ => JsError("cannot parse it")
    }
    def writes(g: TipoUsuario) = JsString(g)
  }

  // TipoReserva
  implicit object tiporeservaFormat extends Format[TipoReserva] {
    def reads(json: JsValue) = json match {
      case JsString(x) => JsSuccess(x)
      case _ => JsError("cannot parse it")
    }
    def writes(g: TipoReserva) = JsString(g)
  }

  // Metodo
  implicit object metodoFormat extends Format[Metodo] {
    def reads(json: JsValue) = json match {
      case JsString(x) => JsSuccess(x)
      case _ => JsError("cannot parse it")
    }
    def writes(g: Metodo) = JsString(g)
  }

  // Coche
  implicit val cocheFormat = Json.format[Coche]

  // Usuario
  implicit val usuarioFormat = Json.format[Usuario]

  // Franquicia
  implicit val franquiciaFormat = Json.format[Franquicia]

  // Reserva
  implicit val reservaFormat = Json.format[Reserva]

  // Factura
  implicit val facturaFormat = Json.format[Factura]

  // Pago
  implicit val pagoFormat = Json.format[Pago]

  // TarifaBase
  implicit val tarifaBaseFormat = Json.format[TarifaBase]

  // TarifaPeriodo
  implicit val tarifaPeriodoFormat = Json.format[TarifaPeriodo]

  // TarifaYoqsetioxdxd
  implicit val tarifaYoqsetioxdxdFormat = Json.format[TarifaYoqsetioxdxd]

  // Extra
  implicit val extraFormat = Json.format[Extra]
}
