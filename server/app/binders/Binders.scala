package binders

import play.api.mvc._
import java.sql.Date
import shared.classes.{Gama, TipoReserva}
import Gama._
import TipoReserva._

object Binders {
  // DATA BINDERS
  implicit object queryStringDateBinder extends QueryStringBindable.Parsing[Date](
    // dateString => DateTimeFormat.forPattern(format).parseLocalDate(dateString),
    dateString => Date.valueOf(dateString),
    _.toString(),
    (key: String, e: Exception) => "Cannot parse parameter %s as java.sql.Date: %s".format(key, e.getMessage)
  )

  implicit object pathDateBinder extends PathBindable.Parsing[Date](
    // dateString => DateTimeFormat.forPattern(format).parseLocalDate(dateString),
    dateString => Date.valueOf(dateString),
    _.toString(),
    (key: String, e: Exception) => "Cannot parse parameter %s as java.sql.Date: %s".format(key, e.getMessage)
  )

  implicit object pathGamaBinder extends PathBindable.Parsing[Gama](
    x => x,
    x => x,
    (key: String, e: Exception) => "Cannot parse parameter %s as Gama: %s".format(key, e.getMessage)
  )

  implicit object pathTipoReservaBinder extends PathBindable.Parsing[TipoReserva](
    x => x,
    x => x,
    (key: String, e: Exception) => "Cannot parse parameter %s as TipoReserva: %s".format(key, e.getMessage)
  )
}
