package test.db

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.util.{Try, Success, Failure}

import play.api.Play
import play.api.Application
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfig
import play.api.test.PlaySpecification
import play.api.test.WithApplication
import slick.driver.JdbcProfile
import javax.inject.Inject
import tables.ExtraDB
import shared.dbclasses.Extra
import shared.classes.Estado
import Estado._
import aux.Implicits._
import org.specs2.mutable.Specification
import java.util.Base64

class AvailabilityHighSpec extends Specification {

  def extrasDB(implicit app: Application) = {
    val app2ExtrasDB = Application.instanceCache[ExtraDB]
    app2ExtrasDB(app)
  }

  val defaultExtras = Seq(Extra("extra1", 10.0f, "no se que cojones poner wei"))

  "default extras as expetec" in new WithApplication {
    val extras = Await.result(extrasDB.list(), 500 millis)

    extras must_== defaultExtras
  }

  "get extra work as expetec" in new WithApplication {
    val extra = Await.result(extrasDB.get("extra1"), 500 milli)

    extra must_== 10.0
  }
}
