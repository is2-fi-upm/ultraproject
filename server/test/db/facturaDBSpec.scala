package test.db

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.util.{Try, Success, Failure}

import play.api.Play
import play.api.Application
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfig
import play.api.test.PlaySpecification
import play.api.test.WithApplication
import slick.driver.JdbcProfile
import javax.inject.Inject
import tables.FacturaDB
import shared.dbclasses.Factura
import aux.Implicits._
import org.specs2.mutable.Specification
import java.util.Base64
import java.sql.Timestamp
import java.text.SimpleDateFormat

class FactSpec extends Specification {
  "User DatabaseConfigProvider" should {

    def factDB(implicit app: Application) = {
      val app2UsuarioDB = Application.instanceCache[FacturaDB]
      app2UsuarioDB(app)
    }

    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'")

    val defaultFact = Seq(
      Factura(Some(1),"Rock", "01211255L",
        new Timestamp(format.parse("2015-11-09T13:37:00.00Z").getTime),
        "1778BGN", 123.2f, "metalico", 1),
      Factura(Some(2), "IS2Company", "W12345",
        new Timestamp(format.parse("2015-11-09T13:37:00.00Z").getTime),
        "8273CNP", 123.2f, "metalico", 1))

    "default factures as expetec" in new WithApplication {
      val factures = Await.result(factDB.list(), 500 millis)

      factures must_== defaultFact
    }

    "insert work as expetec" in new WithApplication {
      val testFact = Factura(Some(3), "Yago", "1234432Z",
        new Timestamp(format.parse("2015-11-10T13:37:00.00Z").getTime),
        "8273CNP", 123.2f, "metalico", 1)

      Await.result(factDB.insert(testFact), 500 millis)

      val users = Await.result(factDB.list(), 500 millis)

      users must_== (defaultFact :+ testFact)
    }

    "getFactura work as expetec" in new WithApplication {
       val user = Await.result(factDB.getFactura(1), 500 millis)

      user must_== Some(Factura(Some(1),"Rock", "01211255L",
        new Timestamp(format.parse("2015-11-09T13:37:00.00Z").getTime),
        "1778BGN", 123.2f, "metalico", 1))
    }

    "insert and get work expetec" in new WithApplication {
      val fact = Await.result(factDB.getFactura(3), 500 millis)
      fact must_== None

      val testFact = Factura(Some(3), "Yago", "1234432Z",
        new Timestamp(format.parse("2015-11-10T13:37:00.00Z").getTime),
        "8273CNP", 123.2f, "metalico", 1)

      Await.result(factDB.insert(testFact), 500 millis)

      val facts = Await.result(factDB.list(), 500 millis)

      facts must_== (defaultFact :+ testFact)
    }

    "listUsuario work as expetec" in new WithApplication {
      val user = Await.result(factDB.listUsuario("01211255L"), 500 millis)

      user must_== Seq(Factura(Some(1),"Rock", "01211255L",
        new Timestamp(format.parse("2015-11-09T13:37:00.00Z").getTime),
        "1778BGN", 123.2f, "metalico", 1))
    }
  }
}
