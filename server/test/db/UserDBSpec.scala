package test.db

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.util.{Try, Success, Failure}

import play.api.Play
import play.api.Application
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfig
import play.api.test.PlaySpecification
import play.api.test.WithApplication
import slick.driver.JdbcProfile
import javax.inject.Inject
import tables.UsuarioDB
import shared.dbclasses.Usuario
import shared.classes.{TipoUsuario, Estado}
import TipoUsuario._
import Estado._
import aux.Implicits._
import org.specs2.mutable.Specification
import java.util.Base64

class UserSpec extends Specification {
  "User DatabaseConfigProvider" should {

    def userDB(implicit app: Application) = {
      val app2UsuarioDB = Application.instanceCache[UsuarioDB]
      app2UsuarioDB(app)
    }

    def contains[T](l1: Seq[T], l2: Seq[T]) = {
      l2 foreach { c => l1 should contain (c) }
    }

    val someUsers = List(
      Usuario("admin", "Rock", "Neurotiko", Admin, "rockneurotiko@gmail.com", "1234"),
      Usuario("W12345", "IS2Company", "Yeah", Empresa, "ceo@company.com", "12345"),
      Usuario("111", "Usuario", "Awesome", Normal, "my@gmail.com", "1234"))

    "default users as expetec" in new WithApplication {
      val users = Await.result(userDB.list(), 500 millis)

      contains(users, someUsers)
    }

    "insert work as expetec" in new WithApplication {
      val testUser = Usuario("29437B","Pene","Castanias",Normal,"pene@toduro.org","1234")

      Await.result(userDB.insert(testUser), 500 millis)

      val users = Await.result(userDB.list(), 500 millis)

      contains(users, someUsers :+ testUser)
    }

    "get work as expetec" in new WithApplication {
       val user = Await.result(userDB.getUser("111"), 500 millis)

      user must_== Some(Usuario("111", "Usuario", "Awesome", Normal, "my@gmail.com", "1234"))
    }

    "insert and get work expetec" in new WithApplication {
      val user = Await.result(userDB.getUser("29437B"), 500 millis)
      user must_== None

      val testUser = Usuario("29437B","Pene","Castanias",Normal,"pene@toduro.org","1234")
      Await.result(userDB.insert(testUser), 500 millis)

      val userRes = Await.result(userDB.getUser("29437B"), 500 millis)

      userRes must_== Some(testUser)
    }

    "Authenticate with valid credentials should work as expetec" in new WithApplication {
      val user = userDB.authenticate("111","1234")

      user must_== Some(Usuario("111", "Usuario", "Awesome", Normal, "my@gmail.com", "1234"))
    }

    "Authentiate with non-valid credentials should work as expetec" in new WithApplication {
      val user = userDB.authenticate("12345567789", "SoyElNinioMasBonitoooo")

      user must_== None
    }
  }
}
