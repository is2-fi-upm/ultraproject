package test.db

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt

import play.api.Play
import play.api.Application
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfig
import play.api.test.PlaySpecification
import play.api.test.WithApplication
import slick.driver.JdbcProfile
import javax.inject.Inject
import tables.CocheDB
import shared.dbclasses.Coche
import shared.classes.{Gama, Estado}
import Gama._
import Estado._
import aux.Implicits._
import org.specs2.mutable.Specification
import java.util.Base64
// import javax.sql.rowset.serial.SerialBlob

class CocheSpec  extends Specification {
  "Car DatabaseConfigProvider" should {

    def cocheDB(implicit app: Application) = {
      val app2CocheDB = Application.instanceCache[CocheDB]
      app2CocheDB(app)
    }

    def contains[T](l1: Seq[T], l2: Seq[T]) = {
      l2 foreach { c => l1 should contain (c) }
    }

    // val blob = new SerialBlob(Base64.getDecoder.decode("UFVUTyBDQU5P"))

    // def removeBlob(c: Coche) = c.copy(foto = blob)

    val someCars = List(Coche("8273CNP", "Citroen", "Saxo",
      Alta, "saxo.jpg", Carretera, 1),
      Coche("9889DBB", "Ford", "Focus", Media, "focus.jpg", Disponible, -1))

    "default cars as expetec" in new WithApplication {
      val coches = Await.result(cocheDB.list(), 500 millis)

      contains(coches, someCars)
    }

    "insert work as expetec" in new WithApplication {
      val testCar = Coche("123", "aaa", "bbb", Alta, "test.jpg", Disponible, -1)

      Await.result(cocheDB.insert(testCar), 500 millis)

      val coches = Await.result(cocheDB.list(), 500 millis)

      contains(coches, someCars :+ testCar)
    }

    "get work as expetec" in new WithApplication {
      val cochenone = Await.result(cocheDB.getCoche("123"), 500 millis)

      cochenone must_== None

      val cochesome = Await.result(cocheDB.getCoche("8273CNP"), 500 millis)

      cochesome must_== Some(Coche("8273CNP", "Citroen", "Saxo", Alta, "saxo.jpg", Carretera, 1))
    }

    "insert and get work as expetec" in new WithApplication {
      val coche = Await.result(cocheDB.getCoche("123"), 500 millis)

      coche must_== None

      val testCar = Coche("123", "aaa", "bbb", Alta, "test.jpg", Disponible, -1)

      Await.result(cocheDB.insert(testCar), 500 millis)

      val coche2 = Await.result(cocheDB.getCoche("123"), 500 millis)

      coche2 must_== Some(testCar)
    }

  }
}
