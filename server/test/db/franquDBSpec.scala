package test.db

import scala.concurrent.Await
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.util.{Try, Success, Failure}

import play.api.Play
import play.api.Application
import play.api.db.slick.DatabaseConfigProvider
import play.api.db.slick.HasDatabaseConfig
import play.api.test.PlaySpecification
import play.api.test.WithApplication
import slick.driver.JdbcProfile
import javax.inject.Inject
import tables.FranquiciaDB
import shared.dbclasses.Franquicia
import shared.dbclasses.Coche
import shared.classes.{Gama, Estado}
import Gama._
import Estado._
import aux.Implicits._
import org.specs2.mutable.Specification
import java.util.Base64
import java.sql.Timestamp
import java.text.SimpleDateFormat

class FranqSpec extends Specification {
  "User DatabaseConfigProvider" should {

    def franqDB(implicit app: Application) = {
      val app2UsuarioDB = Application.instanceCache[FranquiciaDB]
      app2UsuarioDB(app)
    }

    val format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS'Z'")

    val defaultFranq = Seq(Franquicia(Some(1), "Barajas T4", 23, 28080,
      "Madrid", "Madrid", "España"),
      Franquicia(Some(2), "Serrano", 1, 42230, "Zaragoza", "Zaragoza", "España"),
      Franquicia(Some(3), "Facultad de Informatica", 1337, 1903, "Campus Montegancedo", "Madrid", "España"))

    "default franqs as expetec" in new WithApplication {
      val factures = Await.result(franqDB.list(), 500 millis)

      factures must_== defaultFranq
    }

    "insert work as expetec" in new WithApplication {
      val testFranq = Franquicia(None, "C/La Piruleta", 23, 28080,
        "Potorrocity", "Wat?", "Cipotelandia")

      Await.result(franqDB.insert(testFranq), 500 millis)

      val franq = Await.result(franqDB.list(), 500 millis)

      franq must_== (defaultFranq :+ testFranq.copy(id=Some(4)))
    }

    "getFranquicia work as expetec" in new WithApplication {
       val franq = Await.result(franqDB.getFranquicia(1), 500 millis)

      franq must_== Some(Franquicia(Some(1), "Barajas T4", 23, 28080,
        "Madrid", "Madrid", "España"))
    }

    "insert and get work expetec" in new WithApplication {
      val franq = Await.result(franqDB.getFranquicia(4), 500 millis)
      franq must_== None

      val testFranq = Franquicia(None, "C/La Piruleta", 23, 28080,
        "Potorrocity", "Wat?", "Cipotelandia")

      Await.result(franqDB.insert(testFranq), 500 millis)

      val franqs = Await.result(franqDB.list(), 500 millis)

      franqs must_== (defaultFranq :+ testFranq.copy(id=Some(4)))
    }

    "getCoches work as expetec" in new WithApplication {
      val users = Await.result(franqDB.getCoches(1), 500 millis)

      // val blob = new SerialBlob(Base64.getDecoder.decode("UFVUTyBDQU5P"))

      // def removeBlob(c: Coche) = c.copy(foto = blob)

      val expetec = List(Coche("8273CNP", "Citroen", "Saxo",
        Alta, "saxo.jpg", Carretera, 1),
        Coche("9889DBB", "Ford", "Focus", Media, "focus.jpg", Disponible, -1))
      expetec foreach { c => users should contain (c)}
    }

    "getPaises work as expetec" in new WithApplication {
      val paises = Await.result(franqDB.getPaises, 500 millis)

      paises must_== Seq("España")
    }

    "getComunidades work as expetec" in new WithApplication {
      val comunidades = Await.result(franqDB.getComunidades("España"), 500 millis)

      comunidades must_== Vector("Madrid","Zaragoza")
    }

    "getPoblaciones work as expetec" in new WithApplication {
      val poblaciones = Await.result(franqDB.getPoblaciones("España","Madrid"), 500 millis)

      poblaciones must_== Seq("Madrid", "Campus Montegancedo")
    }

    "listWithFilter work as expetec" in new WithApplication {
      val franqis =  Await.result(franqDB.listWithFilter("España","Madrid","Madrid"),
        500 millis)

      franqis must_== Seq(Franquicia(Some(1), "Barajas T4", 23, 28080,
        "Madrid", "Madrid", "España"))
    }
  }
}
