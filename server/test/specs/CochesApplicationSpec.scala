package test.specs

import play.api.test.FakeRequest
import play.api.test.PlaySpecification
import play.api.test.WithApplication
import play.api.libs.json._
import play.api.libs.json.Json
import play.api.test.FakeHeaders
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.mvc._
import scala.concurrent._
import scala.concurrent.duration._
import shared.classes.{Gama, Estado}
import Gama._
import Estado._
import aux.Implicits._

/**
  * Add your spec here.
  * You can mock out a whole application including requests, plugins etc.
  * For more information, consult the wiki.
  */
class CochesApplicationSpec extends PlaySpecification {

  def LoginAdmin(): Cookie = {
    val result = route(FakeRequest(POST, "/login", headers=FakeHeaders(Seq(("Content-Type" -> "application/x-www-form-urlencoded"))), body="dni=admin&password=1234")).get
    val result2 = Await.result(result, 2 seconds)
    val cookie = result2.header.headers("Set-Cookie")
    Cookies.decodeSetCookieHeader(cookie)(1)
  }

  "CochesApplication" should {
    "send 404 on a bad request" in new WithApplication {
      val result = route(FakeRequest(GET, "/not/found")).get
      status(result) mustEqual NOT_FOUND
    }

    "return the values inserted" in new WithApplication {
      val cookie = LoginAdmin()

      val saxito = Json.obj(
        "matricula" -> "1234",
        "marca" -> "Citroen",
        "modelo" -> "Saxo",
        "gama" -> "alta",
        "foto" -> "saxo.jpg",
        "estado" -> "disponible",
        "reserva" -> -1)
      val postRequest = FakeRequest(
        method = "POST",
        uri = "/api/v1/coches",
        headers = FakeHeaders(
          Seq("Content-type"-> "application/json")
        ),
        body =  saxito
      ).withCookies(cookie)

      val result = route(postRequest).get

      status(result) mustEqual OK

      val getPetition = route(FakeRequest(GET, "/api/v1/coches")).get

      status(getPetition) mustEqual OK
      contentType(getPetition) must beSome.which(_ == "application/json")

      val parsed = Json.parse(contentAsString(getPetition))
      val json = parsed(parsed.as[JsArray].value.size - 1)

      val matricula = (json \ "matricula").as[String]
      val marca = (json \ "marca").as[String]
      val modelo = (json \ "modelo").as[String]
      val gama = (json \ "gama").as[Gama]
      val estado = (json \ "estado").as[Estado]
      val reserva = (json \ "reserva").as[Int]

      matricula must equalTo("1234")
      marca must equalTo("Citroen")
      modelo must equalTo("Saxo")
      gama must equalTo(Alta)
      estado must equalTo(Disponible)
      reserva must equalTo(-1)
    }
  }
}
