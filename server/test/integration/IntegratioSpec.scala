package test.integration

import org.specs2.mutable._

import play.api.test._
import play.api.test.Helpers._

/**
  * add your integration spec here.
  * An integration test will fire up a whole play application in a real (or headless) browser
  */
class IntegrationSpec extends Specification {
  "CocheApplication" should {
    "work from within a browser" in {
      val port = 3336
      running(TestServer(port), HTMLUNIT) { browser =>
        browser.goTo("http://localhost:"+port+"/api/v1/coches")
        browser.pageSource must contain("8273CNP")
      }
      true must equalTo(true)
    }
  }
}
