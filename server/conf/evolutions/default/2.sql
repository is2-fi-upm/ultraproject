# --- !Ups

-- TEST DATA

insert into COCHE values ('8273CNP', 'Citroen', 'Saxo', 'alta', 'saxo.jpg', 'carretera', 1);
insert into COCHE values ('9889DBB', 'Ford', 'Focus', 'media', 'focus.jpg', 'disponible', -1);
insert into COCHE values ('6776XDD', 'Ferrari', 'F80', 'alta', 'ferraryf80.jpg', 'disponible', -1);
insert into COCHE values ('6969HDP', 'Mazda', '3', 'media', 'mazda3.jpg', 'carretera', 2);
insert into COCHE values ('0600T', 'Seat', '600', 'baja', '600.jpg', 'disponible', -1);
insert into COCHE values ('0911WTF', 'Porshe', '911', 'alta', 'porsche911.jpg', 'disponible', -1);
insert into COCHE values ('0000A', 'Tronco', 'Movil', 'baja', 'troncomovil.jpg', 'disponible', -1);
insert into COCHE values ('BATMOVIL', 'Batmovil', 'Batman & Robin Edition', 'alta', 'batmovil.jpg', 'disponible', -1);
insert into COCHE values ('0404CCC', 'Citroen', 'C4', 'media', 'c4.jpg', 'carretera', 3);
insert into COCHE values ('7777HMR', 'Hummer', 'H2', 'alta', 'hummer.jpg', 'disponible', -1);


insert into USUARIO values ('admin', 'Rock', 'Neurotiko', 'admin', 'rockneurotiko@gmail.com', '1234');

insert into USUARIO values ('empleado1', 'MyEmployee1', 'Yeah1', 'empleado', 'employee1@gmail.com', '1234');
insert into USUARIO values ('empleado2', 'MyEmployee2', 'Yeah2', 'empleado', 'employee2@gmail.com', '1234');
insert into USUARIO values ('empleado3', 'MyEmployee3', 'Yeah3', 'empleado', 'employee3@gmail.com', '1234');

insert into USUARIO values ('W12345', 'IS2Company', 'Yeah', 'empresa', 'ceo@company.com', '12345');
insert into USUARIO values ('111', 'Usuario', 'Awesome', 'normal', 'my@gmail.com', '1234');

insert into FRANQUICIA values (1, 'Barajas T4', 23, 28080, 'Madrid', 'Madrid', 'España');
insert into FRANQUICIA values (2, 'Serrano', 1, 42230, 'Zaragoza', 'Zaragoza', 'España');
insert into FRANQUICIA values (3, 'Facultad de Informatica', 1337, 1903, 'Campus Montegancedo', 'Madrid', 'España');

insert into COCHESFRANQUICIA ("FRANQUICIA", "COCHE") values (1, '8273CNP');
insert into COCHESFRANQUICIA ("FRANQUICIA", "COCHE") values (1, '9889DBB');
insert into COCHESFRANQUICIA ("FRANQUICIA", "COCHE") values (1, '6776XDD');
insert into COCHESFRANQUICIA ("FRANQUICIA", "COCHE") values (1, '0911WTF');
insert into COCHESFRANQUICIA ("FRANQUICIA", "COCHE") values (2, '0600T');
insert into COCHESFRANQUICIA ("FRANQUICIA", "COCHE") values (2, '0000A');
insert into COCHESFRANQUICIA ("FRANQUICIA", "COCHE") values (3, 'BATMOVIL');
insert into COCHESFRANQUICIA ("FRANQUICIA", "COCHE") values (3, '777HMR');
insert into COCHESFRANQUICIA ("FRANQUICIA", "COCHE") values (3, '6969HDP');
insert into COCHESFRANQUICIA ("FRANQUICIA", "COCHE") values (3, '0404CCC');


insert into EMPLEADOFRANQUICIA values ('empleado1', 1);
insert into EMPLEADOFRANQUICIA values ('empleado2', 2);
insert into EMPLEADOFRANQUICIA values ('empleado3', 3);


insert into RESERVA ("IDUSUARIO", "IDRESERVANTE", "IDCOCHE", "TARJETA", "CADUCIDADTARJETA", "TIPOTARJETA", "FECHARESERVA", "FECHAINICIO", "TIPORESERVA", "PRECIO") values ('01211255L', 'W12345', '8273CNP', '5644687687', '2018-03-01', 'mastercard', {ts '2015-11-09 09:32:03.69'}, '2015-11-25', 'dia', 123.2);
insert into RESERVA ("IDUSUARIO", "IDRESERVANTE", "IDCOCHE", "TARJETA", "CADUCIDADTARJETA", "TIPOTARJETA", "FECHARESERVA", "FECHAINICIO", "TIPORESERVA", "PRECIO") values ('W12345', 'W12345', '6969HDP', '5644687687', '2018-03-01', 'mastercard', {ts '2015-11-09 09:32:03.69'}, '2015-11-25', 'dia', 200.2);
insert into RESERVA ("IDUSUARIO", "IDRESERVANTE", "IDCOCHE", "TARJETA", "CADUCIDADTARJETA", "TIPOTARJETA", "FECHARESERVA", "FECHAINICIO", "TIPORESERVA", "PRECIO") values ('111', '111', '0404CCC', '5644687687', '2018-03-01', 'mastercard', {ts '2015-11-09 09:32:03.69'}, '2015-11-25', 'dia', 300.2);

insert into FACTURA ("NOMBRE", "DNI", "FECHA", "COCHE", "PRECIO", "METODO", "FRANQUICIA") values ('Rock', '01211255L', {ts '2015-11-09 13:37:00.00'}, '1778BGN', 123.2, 'metalico', 1);
insert into FACTURA ("NOMBRE", "DNI", "FECHA", "COCHE", "PRECIO", "METODO", "FRANQUICIA") values ('IS2Company', 'W12345', {ts '2015-11-09 13:37:00.00'}, '8273CNP', 123.2, 'metalico', 1);

insert into PAGO ("FECHA", "RESERVA", "METODO", "COSTE") values ('2014-11-09', 12, 'tarjeta', 123.2);
insert into PAGO ("FECHA", "RESERVA", "METODO", "COSTE") values ('2015-11-09', 13, 'metalico', 223.2);
insert into PAGO ("FECHA", "RESERVA", "METODO", "COSTE") values ('2015-11-10', 14, 'mastercard', 353.2);

insert into TARIFABASE values ('baja', 1.0, 15.0, 90.0, 28.0);
insert into TARIFABASE values ('media', 2.0, 30.0, 180.0, 56.0);
insert into TARIFABASE values ('alta', 4.0, 60.0, 360.0, 112.0);

insert into TARIFAPERIODO values ('semestre1-2015', '2015-01-01', '2015-06-30', 1.2);
insert into TARIFAPERIODO values ('verano-2015', '2015-07-01', '2015-08-30', 1.5);
insert into TARIFAPERIODO values ('fin-2015', '2015-09-01', '2015-12-31', 1.3);

insert into TARIFAYOQSETIOXDXD values ('chicle', 1.2);
insert into TARIFAYOQSETIOXDXD values ('gps', 30.0);
insert into TARIFAYOQSETIOXDXD values ('seguro-para-ricos', 150.0);
insert into TARIFAYOQSETIOXDXD values ('silla-enanos', 10.0);

insert into EXTRA values ('extra1', 10.0, 'no se que cojones poner wei');
# --- !Downs

delete from COCHE;
delete from USUARIO;
delete from FRANQUICIA;
delete from COCHESFRANQUICIA;
delete from EMPLEADOFRANQUICIA;
delete from RESERVA;
delete from FACTURA;
delete from PAGO;
delete from TARIFABASE;
delete from TARIFAPERIODO;
delete from TARIFAYOQSETIOXDXD;
delete from EXTRA;
